package org.mineacademy.nms.enchant;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompSound;

import lombok.Getter;

public final class BlackNovaEnchant extends SimpleEnchantment {

	@Getter
	private static final BlackNovaEnchant instance = new BlackNovaEnchant();

	private BlackNovaEnchant() {
		super("Black Nova", 10);
	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player))
			return;

		final Player player = (Player) event.getDamager();
		final ItemStack handItem = player.getItemInHand();
		final Entity victim = event.getEntity();

		// Useful for debugging whether you properly implemented NMS - both must return true
		//System.out.println("Item has black nova: Our ? " + SimpleEnchantment.hasEnchantment(handItem, BlackNovaEnchant.getInstance()) + " vs Bukkit ? "
		//		+ handItem.containsEnchantment(BlackNovaEnchant.getInstance().toBukkit()));

		if (this.hasEnchant(handItem)) {
			CompSound.ENTITY_BLAZE_HURT.play(victim.getLocation());
			CompParticle.CRIT.spawn(victim.getLocation());

			victim.setFireTicks(3 * 20); // set victim on fire for 3 seconds
		}
	}
}
