package org.mineacademy.nms.enchant;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.nms.model.Effects;

import de.slikey.effectlib.effect.TornadoEffect;
import lombok.Getter;

/**
 * A customized enchantment you can apply to any item.
 */
public final class HideEnchant extends SimpleEnchantment {

	/**
	 * The instance of this class. We also use this to automatically
	 * find and register this enchant when you plugin starts.
	 */
	@Getter
	private static final SimpleEnchantment instance = new HideEnchant();

	/**
	 * Register this enchant with the given name and a maximum level.
	 */
	private HideEnchant() {
		super("Hide", 5);
	}

	/**
	 * Called automatically only when the shooter has this particular enchant on his hand item
	 *
	 * @see org.mineacademy.fo.enchant.SimpleEnchantment#onShoot(int, org.bukkit.entity.LivingEntity, org.bukkit.event.entity.ProjectileLaunchEvent)
	 */
	@Override
	protected void onShoot(final int level, final LivingEntity shooter, final ProjectileLaunchEvent event) {
		if (!(shooter instanceof Player))
			return;

		final Player player = (Player) shooter;
		CompSound.BLOCK_ANVIL_LAND.play(player);

		final Projectile projectile = event.getEntity();

		final TornadoEffect effect = new TornadoEffect(Effects.getEffectManager());

		effect.iterations = -1;
		effect.tornadoParticle = Particle.CRIT;

		EntityUtil.trackFlying(projectile, () -> {

			// 1 and 2nd way - using NMS
			//CompParticle.BLOCK_CRACK.spawnWithData(loc, CompMaterial.BLUE_WOOL);
			/*final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(Particles.HAPPY_VILLAGER, false,
					loc.getX(), loc.getY(), loc.getZ(), 0, 0, 0, 0, 1);
			
			for (final Player onlinePlayer : Remain.getOnlinePlayers())
				Remain.sendPacket(onlinePlayer, packet);
			//((CraftPlayer) onlinePlayer).getHandle().playerConnection.sendPacket(packet);*/

			// 3rd way using EffectLib
			final Location loc = projectile.getLocation();

			effect.setLocation(loc);
			effect.run();
		});

		// 4th way - show particle to player continuously
		/*final Player player = getPlayer();
		
		final Effect effect = new de.slikey.effectlib.effect.DiscoBallEffect(Effects.getEffectManager());
		
		effect.period = 2; // the pause between iterations
		effect.iterations = 30; // how many times until the end
		
		effect.setEntity(getPlayer());
		effect.start();*/
	}

	/**
	 * Called automatically only when the shooter had this particular enchant on his hand item
	 *
	 * @see org.mineacademy.fo.enchant.SimpleEnchantment#onHit(int, org.bukkit.entity.LivingEntity, org.bukkit.event.entity.ProjectileHitEvent)
	 */
	@Override
	protected void onHit(final int level, final LivingEntity shooter, final ProjectileHitEvent event) {
		final Entity hitEntity = event.getHitEntity();

		if (hitEntity instanceof LivingEntity)
			((LivingEntity) hitEntity).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, level * 3 * 20, 0));
	}
}