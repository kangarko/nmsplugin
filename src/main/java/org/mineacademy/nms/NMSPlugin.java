package org.mineacademy.nms;

import java.util.Arrays;
import java.util.List;

import org.mineacademy.fo.Common;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.MinecraftVersion.V;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.settings.Lang;
import org.mineacademy.fo.settings.SimpleLocalization;
import org.mineacademy.fo.settings.YamlStaticConfig;
import org.mineacademy.nms.block.BlockRegistry;
import org.mineacademy.nms.block.BlockTask;
import org.mineacademy.nms.citizens.KillBossTrait;
import org.mineacademy.nms.command.NMSCommand;
import org.mineacademy.nms.model.Effects;
import org.mineacademy.nms.npc.NPCRegistry;
import org.mineacademy.nms.npc.NPCTask;
import org.mineacademy.nms.settings.Settings;
import org.mineacademy.nms.specific.enchant.CustomEnchant_v1_18;
import org.mineacademy.nms.specific.enchant.CustomEnchant_v1_19;
import org.mineacademy.nms.specific.enchant.CustomEnchant_v1_20_R3;
import org.mineacademy.nms.specific.enchant.CustomEnchant_v1_8;

import lombok.Getter;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;

/**
 * PluginTemplate is a simple template you can use every time you make
 * a new plugin. This will save you time because you no longer have to
 * recreate the same skeleton and features each time.
 * <p>
 * It uses Foundation for fast and efficient development process.
 */
public final class NMSPlugin extends SimplePlugin {

	/**
	 * Automatically load static settings classes. Settings in these classes can only be modified during startup.
	 * If you need to save/edit data during gameplay, such as in minigames plugins, use YamlConfig instead.
	 */
	@Getter
	private final List<Class<? extends YamlStaticConfig>> settings = Arrays.asList(Settings.class, SimpleLocalization.class);

	@Override
	protected void onPluginLoad() {
		final int subVersion = MinecraftVersion.getSubversion();

		if (MinecraftVersion.newerThan(V.v1_20))
			return; // TODO Not supported yet

		if (MinecraftVersion.equals(V.v1_20)) {
			if (subVersion == 4)
				SimpleEnchantment.registerEnchantmentHandle(CustomEnchant_v1_20_R3.class);
			else if (subVersion == 5 || subVersion == 6)
				System.out.println("Custom enchants for 1.20.5/6 not implemented yet");
			else
				throw new RuntimeException("Custom enchants require Minecraft 1.20.4 or 1.20.5");

		} else if (MinecraftVersion.atLeast(V.v1_19)) {
			if (subVersion == 4)
				SimpleEnchantment.registerEnchantmentHandle(CustomEnchant_v1_19.class);
			else
				throw new RuntimeException("Custom enchants require Minecraft 1.19.4");

		} else if (MinecraftVersion.atLeast(V.v1_13))
			SimpleEnchantment.registerEnchantmentHandle(CustomEnchant_v1_18.class);

		else if (MinecraftVersion.equals(V.v1_8))
			SimpleEnchantment.registerEnchantmentHandle(CustomEnchant_v1_8.class);
	}

	/**
	 * Automatically perform login ONCE when the plugin starts.
	 */
	@Override
	protected void onPluginStart() {
		// Button.<static method>
		// Menu.<static method>
		//ButtonReturnBack.<static method>

		if (HookManager.isCitizensLoaded())
			CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(KillBossTrait.class).withName("kill-boss"));

		Common.runLater(() -> {
			NPCRegistry.getInstance().spawnFromDisk();
			BlockRegistry.getInstance();
		});
	}

	/**
	 * Automatically perform login when the plugin starts and each time it is reloaded.
	 */
	@Override
	protected void onReloadablesStart() {

		// You can check for necessary plugins and disable loading if they are missing
		Valid.checkBoolean(HookManager.isVaultLoaded(), "You need to install Vault so that we can work with packets, offline player data, prefixes and groups.");

		if (Common.doesPluginExist("EffectLib"))
			Effects.load();
		else
			Common.warning("Please install EffectLib plugin for custom effects functionality.");

		// Load localization
		Lang.init();

		// Uncomment to load variables
		// Variable.loadVariables();

		//
		// Add your own plugin parts to load automatically here
		//

		// Register commands
		registerAllCommands(NMSCommand.class);

		//
		// Add your own commands here
		//

		//
		// Add your own events ere
		//

		if (MinecraftVersion.atLeast(V.v1_8))
			Common.runTimer(5, new NPCTask(15, 50));

		if (MinecraftVersion.atLeast(V.v1_13))
			Common.runTimer(1, new BlockTask());
	}

	@Override
	protected void onPluginReload() {
		if (Common.doesPluginExist("EffectLib"))
			Effects.disable();

		NPCRegistry.getInstance().save();
	}

	/**
	 * @see org.mineacademy.fo.plugin.SimplePlugin#onPluginStop()
	 */
	@Override
	protected void onPluginStop() {
		NPCRegistry.getInstance().save();
	}

	/* ------------------------------------------------------------------------------- */
	/* Static */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Return the instance of this plugin, which simply refers to a static
	 * field already created for you in SimplePlugin but casts it to your
	 * specific plugin instance for your convenience.
	 *
	 * @return the main instance
	 */
	public static NMSPlugin getInstance() {
		return (NMSPlugin) SimplePlugin.getInstance();
	}
}
