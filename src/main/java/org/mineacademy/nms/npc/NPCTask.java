package org.mineacademy.nms.npc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.specific.npc.NPC;

import lombok.RequiredArgsConstructor;

/**
 * Represents a self-repeating task managing NPC.
 */
@RequiredArgsConstructor
public final class NPCTask extends SimpleRunnable {

	/**
	 * The range from which NPCs should look at the closest player, automatically
	 */
	private final double lookAtRange;

	/**
	 * The range from which NPCs are visible
	 */
	private final double visibleTresholdBlocks;

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		final NPCRegistry registry = NPCRegistry.getInstance();

		for (final NPC npc : registry.getLoadedNPCs()) {
			this.makeNPCLookAtClosestPlayer(npc);
			this.hideFarAwayPlayers(npc);
			this.showPlayersInRange(npc);
		}
	}

	/*
	 * Makes the given NPC automatically target the closest player to look at, if enabled
	 */
	private void makeNPCLookAtClosestPlayer(final NPC npc) {
		if (npc.isLookingAtPlayer()) {
			final Player closestPlayer = EntityUtil.findNearestEntity(npc.getLocation(), this.lookAtRange, Player.class);

			if (closestPlayer != null)
				npc.setRotation(closestPlayer.getLocation());
		}
	}

	/*
	 * Hides far away or disconnected players from the NPC
	 */
	private void hideFarAwayPlayers(final NPC npc) {
		final Location npcLocation = npc.getLocation();
		final List<Player> playersToHide = new ArrayList<>();

		// Remove players out of range
		for (final UUID viewerUid : npc.getViewers()) {
			final Player player = Remain.getPlayerByUUID(viewerUid);

			// Player offline
			if (player == null) {
				System.out.println("@hiding " + npc + " from offline " + viewerUid);
				npc.removeViewer(viewerUid);

				continue;
			}

			if (!player.getLocation().getWorld().equals(npcLocation.getWorld()) || player.getLocation().distance(npcLocation) > this.visibleTresholdBlocks) {
				playersToHide.add(player);

				System.out.println("@hiding " + npc + " from " + player.getName());
			}
		}

		for (final Player playerToHide : playersToHide)
			npc.hide(playerToHide);
	}

	/*
	 * Shows players within the set range to the NPC
	 */
	private void showPlayersInRange(final NPC npc) {
		final Location npcLocation = npc.getLocation();

		for (final Player online : Remain.getOnlinePlayers()) {
			final Location onlineLocation = online.getLocation();

			if (!npc.isVisibleTo(online) && onlineLocation.getWorld().equals(npcLocation.getWorld()) && onlineLocation.distance(npcLocation) < this.visibleTresholdBlocks) {
				npc.show(online);

				System.out.println("@showing " + npc + " to " + online.getName());
			}
		}
	}
}
