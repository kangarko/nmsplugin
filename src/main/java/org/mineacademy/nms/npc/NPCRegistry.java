package org.mineacademy.nms.npc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.constants.FoConstants;
import org.mineacademy.fo.settings.YamlConfig;
import org.mineacademy.nms.specific.npc.NPC;

import lombok.Getter;

/**
 * Represents an elegant way to permanently store and load NPCs
 */
public final class NPCRegistry extends YamlConfig {

	/**
	 * The singleton of this class
	 */
	@Getter
	private static final NPCRegistry instance = new NPCRegistry();

	/**
	 * Represents currently loaded NPCs
	 */
	private final List<NPC> loadedNPCs = new ArrayList<>();

	/**
	 * Create a new registry and load
	 */
	private NPCRegistry() {
		this.loadConfiguration(NO_DEFAULT, FoConstants.File.DATA);
	}

	/**
	 * Automatically loads stored disk NPCs and spawns them
	 */
	public void spawnFromDisk() {

		if (!MinecraftVersion.atLeast(MinecraftVersion.V.v1_16))
			return;

		// Tricky: This automatically calls the spawn method which puts the NPC to our loadedNPCs list
		this.getList("Saved_NPCs", NPC.class);

		System.out.println("@Found " + this.loadedNPCs.size() + " NPCs on the disk");

		for (final NPC npc : this.loadedNPCs)
			System.out.println("\tspawned " + npc + " at " + npc.getLocation());
	}

	/**
	 * @see org.mineacademy.fo.settings.YamlConfig#serialize()
	 */
	@Override
	protected void onSave() {
		this.save("Saved_NPCs", this.getLoadedNPCs());
	}

	/**
	 * Registers a new NPC to our map
	 *
	 * @param npc
	 */
	public void register(final NPC npc) {
		Valid.checkBoolean(!this.isRegistered(npc), npc + " is already registered!");

		this.loadedNPCs.add(npc);
		this.save();
	}

	/**
	 * Return true if the given NPC is already registered
	 *
	 * @param npc
	 * @return
	 */
	public boolean isRegistered(final NPC npc) {
		return npc.isSpawned() && this.isRegistered(npc.getUniqueId());
	}

	/**
	 * Return true if the given NPC is already registered
	 *
	 * @param entityUniqueId
	 * @return
	 */
	public boolean isRegistered(final UUID entityUniqueId) {
		for (final NPC npc : this.loadedNPCs)
			if (npc.isSpawned() && npc.getUniqueId().equals(entityUniqueId))
				return true;

		return false;
	}

	/**
	 * Get the loaded NPCs
	 * @return
	 */
	public List<NPC> getLoadedNPCs() {
		return Collections.unmodifiableList(loadedNPCs);
	}
}
