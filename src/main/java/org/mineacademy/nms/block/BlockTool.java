package org.mineacademy.nms.block;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.visual.VisualTool;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * An automatically registered tool you can use in the game
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BlockTool extends VisualTool {

	/**
	 * The singular tool instance
	 */
	@Getter
	private static final Tool instance = new BlockTool();

	/**
	 * The actual item stored here for maximum performance
	 */
	private ItemStack item;

	/**
	 * @see Tool#getItem()
	 */
	@Override
	public ItemStack getItem() {

		if (item == null)
			item = ItemCreator.of(
					CompMaterial.NETHER_STAR,
					"Block Tool",
					"",
					"Click blocks to",
					"un/register them.")
					.make();

		return item;
	}

	/**
	 * Cancel the event so that we don't destroy blocks when selecting them
	 *
	 * @see Tool#autoCancel()
	 */
	@Override
	protected boolean autoCancel() {
		return true;
	}

	@Override
	protected void handleBlockClick(final Player player, final ClickType click, final Block block) {
		final BlockRegistry registry = BlockRegistry.getInstance();
		final boolean isRegistered = registry.isRegistered(block);

		if (isRegistered)
			registry.unregister(block);
		else
			registry.register(block);

		Messenger.success(player, "Successfully " + (isRegistered ? "un" : "") + "registered the block at " + Common.shortLocation(block.getLocation()) + ".");
	}

	@Override
	protected List<Location> getVisualizedPoints(final Player player) {
		return BlockRegistry.getInstance().getLocations();
	}

	@Override
	protected String getBlockName(final Block block, final Player player) {
		return "Registered Block";
	}

	@Override
	protected CompMaterial getBlockMask(final Block block, final Player player) {
		return CompMaterial.IRON_BLOCK;
	}
}
