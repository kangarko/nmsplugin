package org.mineacademy.nms.block;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.CompMaterial;

public class BlockTask extends SimpleRunnable {

	// pick a random location
	// call a method for it
	// e.g. > crops grow FASTER than in normal MC

	@Override
	public void run() {
		//System.out.println("Task running");

		// Homework : create a two-phased system
		// Project : shooting blocks

		for (final World world : Bukkit.getWorlds())
			for (final Chunk chunk : world.getLoadedChunks()) {
				final Block randomBlock = chunk.getBlock(
						RandomUtil.nextBetween(0, 15),
						RandomUtil.nextBetween(0, world.getMaxHeight() - 1),
						RandomUtil.nextBetween(0, 15));

				//System.out.println("Ticking " + randomBlock);

				/*if (!BlockVisualizer.isVisualized(randomBlock))
					BlockVisualizer.visualize(randomBlock, CompMaterial.BEACON, "Ticked Right Now");

				Common.runLater(20, () -> {
					if (BlockVisualizer.isVisualized(randomBlock))
						BlockVisualizer.stopVisualizing(randomBlock);
				});*/

				if (BlockRegistry.getInstance().isRegistered(randomBlock)) {
					final Block blockAbove = randomBlock.getRelative(BlockFace.UP);

					if (randomBlock.getType() == CompMaterial.FARMLAND.getMaterial() & blockAbove.getType() == CompMaterial.WHEAT.getMaterial())
						if (blockAbove.getBlockData() instanceof org.bukkit.block.data.Ageable) {
							final org.bukkit.block.data.Ageable ageable = (org.bukkit.block.data.Ageable) blockAbove.getBlockData();

							ageable.setAge(ageable.getMaximumAge());
							blockAbove.setBlockData(ageable);
						}
				}
			}
	}
}
