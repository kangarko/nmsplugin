package org.mineacademy.nms.block;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.mineacademy.fo.SerializeUtil;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.constants.FoConstants;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.settings.YamlConfig;

import lombok.Getter;

public class BlockRegistry extends YamlConfig {

	@Getter
	private static final BlockRegistry instance = new BlockRegistry();

	private final Set<String> registeredBlocks = new HashSet<>();

	private BlockRegistry() {
		this.loadConfiguration(NO_DEFAULT, FoConstants.File.DATA);
	}

	@Override
	protected void onLoad() {
		for (final String hash : this.getStringList("Blocks")) {
			final Tuple<Location, CompMaterial> tuple = fromHash(hash);

			final Location location = tuple.getKey();
			final CompMaterial material = tuple.getValue();

			final Block block = location.getBlock();

			if (block.getType() == material.getMaterial() && block.getData() == material.getData())
				this.registeredBlocks.add(hash);
		}
	}

	@Override
	protected void onSave() {
		this.set("Blocks", this.registeredBlocks);
	}

	public void register(final Block block) {
		final String hash = this.toHash(block.getLocation(), CompMaterial.fromMaterial(block.getType()));
		Valid.checkBoolean(!this.registeredBlocks.contains(hash), block + " has already been registered");

		this.registeredBlocks.add(hash);
		this.save();
	}

	public void unregister(final Block block) {
		final String hash = this.toHash(block.getLocation(), CompMaterial.fromMaterial(block.getType()));
		Valid.checkBoolean(this.registeredBlocks.contains(hash), block + " has not been registered");

		this.registeredBlocks.remove(hash);
		this.save();
	}

	public boolean isRegistered(final Block block) {
		final String hash = this.toHash(block.getLocation(), CompMaterial.fromMaterial(block.getType()));

		return this.registeredBlocks.contains(hash);
	}

	public List<Location> getLocations() {
		final List<Location> locations = new ArrayList<>();

		for (final String hash : this.registeredBlocks)
			locations.add(this.fromHash(hash).getKey());

		return locations;
	}

	private Tuple<Location, CompMaterial> fromHash(final String hash) {
		final String[] split = hash.split(" \\| ");
		final Location location = SerializeUtil.deserializeLocation(split[0]);
		final CompMaterial material = CompMaterial.valueOf(split[1]);

		return new Tuple<>(location, material);
	}

	private String toHash(final Location location, final CompMaterial material) {
		return SerializeUtil.serializeLoc(location) + " | " + material;
	}
}
