package org.mineacademy.nms.command;

import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.model.ChunkedTask;
import org.mineacademy.fo.model.OfflineRegionScanner;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.nms.PlayerCache;
import org.mineacademy.nms.model.RegionScanner;
import org.mineacademy.nms.model.RegionTool;

/**
 * A sample command to open a menu we create in the course.
 */
@SuppressWarnings("unused")
public final class ScanCommand extends NMSCommand {

	public ScanCommand() {
		super("scan");

		setMinArguments(1);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0];
		final PlayerCache cache = PlayerCache.from(getPlayer());

		if ("chunk".equals(param)) {
			final Chunk chunk = getPlayer().getLocation().getChunk();

			final int regionX = chunk.getX() >> 5;
			final int regionZ = chunk.getZ() >> 5;

			final Set<Location> boundingBox = BlockUtil.getBoundingBox(chunk);

			Common.runTimer(10, new SimpleRunnable() {
				int showedCount = 0;

				@Override
				public void run() {
					if (showedCount++ > 10) {
						cancel();

						return;
					}

					for (final Location location : boundingBox)
						CompParticle.VILLAGER_HAPPY.spawn(location);
				}
			});

		} else if ("world".equals(param)) {
			checkArgs(2, "Usage: /{label} {0} <worldName>");

			final World world = Bukkit.getWorld(args[1]);
			checkNotNull(world, "No such world '{1}', available: " + Common.join(Bukkit.getWorlds()));

			// Confirmation to run this command
			final int estimatedWaitTimeSec = OfflineRegionScanner.getEstimatedWaitTimeSec(world) * 2;

			if (!cache.isConfirmedWorldScanCommand()) {
				cache.setConfirmedWorldScanCommand(true);

				returnTell("You will wait approx. " + TimeUtil.formatTimeGeneric(estimatedWaitTimeSec) + " to complete the operation. " +
						"Server will not be usable during this time! Type this command again to confirm and start.");
			}

			final RegionScanner scanner = new RegionScanner();

			scanner.scan(world);
			cache.setConfirmedWorldScanCommand(false);

		} else if ("tool".equals(param))
			RegionTool.getInstance().give(getPlayer());

		else if ("region".equals(param)) {
			checkBoolean(cache.getRegion().isWhole(), "Please select primary and secondary location using the tool first!");

			final List<Block> blocks = cache.getRegion().getBlocks();

			new ChunkedTask(50_000) {

				@Override
				protected void onProcess(final int index) {
					// 0 > 499,999 (or how many blocks you have)

					final Block block = blocks.get(index);
					final Material material = block.getType();

					if (material == Material.AIR)
						block.setType(CompMaterial.ACACIA_LOG.getMaterial());

					if (material == Material.ACACIA_LOG)
						block.setType(CompMaterial.AIR.getMaterial());
				}

				@Override
				protected boolean canContinue(final int index) {
					return index < blocks.size();
				}

				@Override
				protected void onFinish(boolean gracefully) {
					tellSuccess("Finished processing " + blocks.size() + " blocks.");
				}
			}.startChain();
		}
	}
}
