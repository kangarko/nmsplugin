package org.mineacademy.nms.command;

import org.mineacademy.fo.command.SimpleCommand;

/**
 * Represents an in-the-middle command upon which
 * all standard commands in this plugin are built.
 *
 * We can then automatically register all commands
 * extending this class on plugin startup.
 */
public abstract class NMSCommand extends SimpleCommand {

	/**
	 * Create a new {@link NMSCommand} with the given label.
	 *
	 * @param label
	 */
	protected NMSCommand(String label) {
		super(label);
	}

}
