package org.mineacademy.nms.command;

import java.util.List;

import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.nms.specific.npc.NPC;

/**
 * A sample command to spawn custom NPC (fake players/bots).
 */
public final class NPCNMSCommand extends NMSCommand {

	public NPCNMSCommand() {
		super("npc-nms");

		setUsage("<name> [skin]");
		setDescription("Spawn fake players with an optional skin.");
		setMinArguments(1);
	}

	@Override
	protected void onCommand() {
		checkConsole();
		checkBoolean(MinecraftVersion.atLeast(MinecraftVersion.V.v1_8), "This command requires Minecraft 1.8.8+.");

		final String name = args[0];
		final NPC npc = NPC.create(name);

		npc.spawn(getPlayer().getLocation());
		npc.show();

		if (args.length == 2) {
			final String skin = args[1];

			npc.setSkin(skin);
		}
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}
