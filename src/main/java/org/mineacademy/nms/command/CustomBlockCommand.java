package org.mineacademy.nms.command;

import java.util.List;

import org.mineacademy.nms.block.BlockTool;

/**
 * A sample command to open a menu we create in the course.
 */
public final class CustomBlockCommand extends NMSCommand {

	public CustomBlockCommand() {
		super("customblock");

		this.setMinArguments(1);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0];

		if ("tool".equals(param))
			BlockTool.getInstance().give(getPlayer());
	}

	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return this.completeLastWord("tool");

		return NO_COMPLETE;
	}
}
