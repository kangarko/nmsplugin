package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompParticle;

/**
 * A sample command to spawn heart particle animation using AI-generated code.
 */
public final class HeartCommand extends NMSCommand {

	private boolean isAnimating = false;

	public HeartCommand() {
		super("heart");

		this.setMinArguments(1);
		this.setUsage("<start/stop>");
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		// Start spawning heart particles behind the player for 10 seconds
		if (args[0].equals("start")) {
			this.startAnimation();

			tellSuccess("Animation started!");

		} else {
			this.stopAnimation();

			tellSuccess("Animation stopped!");
		}
	}

	private void startAnimation() {
		if (!isAnimating) {
			isAnimating = true;

			new SimpleRunnable() {

				@Override
				public void run() {
					for (final Player player : Bukkit.getOnlinePlayers()) {

						for (double t = 0; t < 2 * Math.PI; t += Math.PI / 128) {
							final double x = 0.15 * (16 * Math.pow(Math.sin(t), 3));
							final double y = 0.15 * (13 * Math.cos(t) - 5 * Math.cos(2 * t) - 2 * Math.cos(3 * t) - Math.cos(4 * t));
							final double z = 0;
							final Location loc = player.getLocation().add(x, y + 1.5, z);

							CompParticle.REDSTONE.spawn(loc, 1);

							loc.subtract(x, y, z);
						}
					}
					if (!isAnimating) {
						cancel();
					}
				}
			}.runTaskTimer(SimplePlugin.getInstance(), 0L, 1L);
		}
	}

	private void stopAnimation() {
		isAnimating = false;
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}
