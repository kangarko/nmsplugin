package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Villager;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.nms.citizens.KillBossTrait;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.ai.Goal;
import net.citizensnpcs.api.ai.Navigator;
import net.citizensnpcs.api.ai.goals.TargetNearbyEntityGoal;
import net.citizensnpcs.api.ai.goals.WanderGoal;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;

/**
 * A sample command to spawn custom NPC (fake players/bots).
 */
public final class NPCCitizensCommand extends NMSCommand {

	public NPCCitizensCommand() {
		super("npc-citizens");

		setUsage("<params... >");
		setMinArguments(1);
	}

	@Override
	protected void onCommand() {
		checkConsole();
		checkBoolean(HookManager.isCitizensLoaded(), "This command requires Citizens plugin to be installed.");

		final NPCRegistry registry = CitizensAPI.getNPCRegistry();
		final String param = args[0];

		final Location playerLocation = getPlayer().getLocation();

		// /npc-citizens create <name>

		if ("create".equals(param)) {
			checkArgs(2, "Please specify the name of the NPC.");

			final String name = args[1];
			final NPC npc = registry.createNPC(EntityType.PLAYER, name);

			npc.setProtected(false);
			npc.spawn(playerLocation);
			tellSuccess("Created NPC with id " + npc.getId() + " with class " + ((CraftPlayer) npc.getEntity()).getHandle());

		} else if ("walk".equals(param) || "follow".equals(param)) {
			final int id = findNumber(1, "Please type a valid number as NPC id.");
			final NPC npc = registry.getById(id);

			checkNotNull(npc, "No such NPC by ID {1}. Available: " + Common.join(registry, ", ", iteratedNPC -> iteratedNPC.getName() + " (" + iteratedNPC.getId() + ")"));

			final Navigator gps = npc.getNavigator();

			if ("walk".equals(param))
				gps.setTarget(playerLocation);
			else if (gps.getEntityTarget() != null)
				gps.cancelNavigation();
			else
				gps.setTarget(getPlayer(), false);

		} else if ("boss".equals(param)) {
			checkArgs(3, "Usage: /{label} {0} <entityType> <npcName>");

			// /npc-citizens boss <entityType> <npcName>
			final EntityType type = findEnum(EntityType.class, args[1], "No such entity type '{1}', available: " + Common.join(EntityType.values()));
			final String name = args[2];

			final NPC npc = registry.createNPC(type, name);
			npc.spawn(playerLocation);

			final LivingEntity entity = (LivingEntity) npc.getEntity();

			npc.setProtected(false);
			entity.setHealth(10);

			npc.data().set(NPC.Metadata.GLOWING, true);
			npc.data().set(NPC.Metadata.DEATH_SOUND, "entity.wither_skeleton.death");

			// Add wandering goal
			final Goal wonderingGoal = WanderGoal.builder(npc).xrange(20).yrange(10).build();
			npc.getDefaultGoalController().addGoal(wonderingGoal, 1);

			final Goal attackGoal = TargetNearbyEntityGoal.builder(npc)
					.aggressive(true)
					.radius(15)
					.targets(Common.newSet(EntityType.PLAYER))
					.build();
			npc.getDefaultGoalController().addGoal(attackGoal, 2);

			npc.getNavigator().getLocalParameters()
					.attackDelayTicks(20)
					.attackRange(25)
					.baseSpeed(1.5F)
					.updatePathRate(4 * 20);

			CompMetadata.setMetadata(entity, "Citizens-Boss", "Citizens-Boss");

			tellSuccess("Created an NPC of type " + ItemUtil.bountifyCapitalized(type) + " with ID " + npc.getId());
		} else if ("quest".equals(param)) {
			final NPC npc = registry.createNPC(EntityType.VILLAGER, "Quester");

			npc.spawn(playerLocation);
			npc.addTrait(new KillBossTrait());

			final Villager villager = (Villager) npc.getEntity();
			villager.setProfession(Villager.Profession.BUTCHER);

		}
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord("create", "boss", "walk", "follow", "quest");

		if (args.length == 2)
			if ("boss".equals(args[0]))
				return completeLastWord(EntityType.values());
			else if ("walk".equals(args[0]) || "follow".equals(args[0])) {
				final List<Integer> ids = Common.convert(CitizensAPI.getNPCRegistry(), NPC::getId);

				return completeLastWord(ids);
			}

		return NO_COMPLETE;
	}
}
