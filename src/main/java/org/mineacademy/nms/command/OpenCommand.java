package org.mineacademy.nms.command;

import java.util.Arrays;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.MinecraftVersion.V;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.MerchantRecipe;
import net.minecraft.server.v1_8_R3.MerchantRecipeList;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

/**
 * A sample command to open a menu we create in the course.
 */
public final class OpenCommand extends NMSCommand {

	public OpenCommand() {
		super("open");

		setUsage("<type>");
		setMinArguments(1);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0];
		final Player player = getPlayer();

		if ("book".equals(param)) {
			/*final ItemStack book = new ItemStack(CompMaterial.WRITTEN_BOOK.getMaterial());
			final BookMeta meta = (BookMeta) book.getItemMeta();

			meta.setAuthor("kangarko");
			meta.setTitle("Demo book");
			meta.setPages(Common.colorize("First &cpage\nanother line", "Second page"));

			book.setItemMeta(meta);*/

			final ItemStack book = ItemCreator.of(CompMaterial.WRITTEN_BOOK)
					.bookAuthor("kangarko")
					.bookTitle("Test book")
					.bookPages(Arrays.asList("First &cpage\nanother line", "Second page"))
					.make();

			Remain.openBook(player, book);

		} else if ("enchant".equals(param))
			// TODO homework to fix this
			player.openEnchanting(player.getLocation(), true);

		else if ("merchant".equals(param)) {

			// You can further expand this to send the inventory open packet and remove the entity afterwards
			if (MinecraftVersion.equals(V.v1_8)) {
				final Location location = player.getLocation();
				final Villager villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);
				final EntityVillager merchant = ((CraftVillager) villager).getHandle();
				final NBTTagCompound nbt = new NBTTagCompound();

				merchant.setPositionRotation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

				merchant.c(nbt); // Prevent instant disappear
				nbt.setInt("NoAI", 1); // Prevent movement
				merchant.f(nbt); // Assign all other nbt properties

				final MerchantRecipeList recipesList = new MerchantRecipeList();
				recipesList.add(new MerchantRecipe(CraftItemStack.asNMSCopy(new ItemStack(Material.CLAY_BALL, 7)), CraftItemStack.asNMSCopy(new ItemStack(Material.SANDSTONE, 6, (byte) 1))));
				ReflectionUtil.setDeclaredField(merchant, "br", recipesList);

			} else {

				// <1.16
				//final EntityVillager villager = new EntityVillager(EntityTypes.aV, (WorldServer) Remain.getHandleWorld(player.getWorld()));

				// 1.17+
				final net.minecraft.world.entity.npc.Villager villager = new net.minecraft.world.entity.npc.Villager(net.minecraft.world.entity.EntityType.VILLAGER,
						(net.minecraft.world.level.Level) Remain.getHandleWorld(player.getWorld()));

				final Villager bukkitEntity = (Villager) villager.getBukkitEntity();

				// Homework:
				// spawn villager + add invisible potion effect + add metadata to player
				// when player closes inventory remove() the villager

				player.openMerchant(bukkitEntity, false);
			}

		} else if ("workbench".equals(param))
			player.openWorkbench(player.getLocation(), true);
		else if ("fake-block".equals(param))
			player.sendBlockChange(Remain.getTargetBlock(player, 15).getLocation(), CompMaterial.DIAMOND_BLOCK.getMaterial(), (byte) 0);

		else if ("fake-exp".equals(param))
			try {
				player.sendExperienceChange(0.33F, 100);
			} catch (final NoSuchMethodError err) {
				tellError("Sending fake exp is not implemented on MC " + MinecraftVersion.getFullVersion());
			}
		else if ("fake-sign".equals(param))
			player.sendSignChange(Remain.getTargetBlock(player, 15).getLocation(), new String[] {
					"first",
					"second",
					"third",
					""
			});
	}
}
