package org.mineacademy.nms.command;

import org.mineacademy.nms.specific.sign.NMSSign;

/**
 * A sample command to open a menu we create in the course.
 */
public final class SignCommand extends NMSCommand {

	public SignCommand() {
		super("sign");

		setUsage("[lines]");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String[] lines = new String[4];

		// LINE ONE
		// LINE TWO
		// LINE THREE
		// LINE FOUR

		// /sign hello world|second line|third line
		final String[] playerLines = joinArgs(0).split("\\|");

		for (int index = 0; index < lines.length; index++)
			lines[index] = index < playerLines.length ? playerLines[index] : "";

		NMSSign.openSign(getPlayer(), lines);

	}
}
