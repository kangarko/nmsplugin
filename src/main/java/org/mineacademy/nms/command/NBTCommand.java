package org.mineacademy.nms.command;

import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.remain.nbt.NBTTileEntity;

/**
 * A sample command to open a menu we create in the course.
 */
@SuppressWarnings("unused")
public final class NBTCommand extends NMSCommand {

	public NBTCommand() {
		super("nbt");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final ItemStack hand = getPlayer().getItemInHand();

		// Way 1: NMS
		/*final net.minecraft.world.item.ItemStack nmsHand = CraftItemStack.asNMSCopy(hand);
		final NBTTagCompound mainTag = nmsHand.hasTag() ? nmsHand.getTag() : new NBTTagCompound();
		final NBTTagList enchantsList = (NBTTagList) mainTag.get("StoredEnchantments");
		
		final NBTTagCompound enchantTag = (NBTTagCompound) enchantsList.get(0);
		enchantTag.setShort("lvl", (short) 10);
		
		nmsHand.setTag(mainTag);
		getPlayer().setItemInHand(CraftItemStack.asBukkitCopy(nmsHand));*		 */

		/*final NBTItem item = new NBTItem(hand);
		System.out.println("Item tag: " + item);
		
		final NBTCompoundList enchants = item.getCompoundList("StoredEnchantments");
		final NBTListCompound enchant = enchants.get(0);
		
		enchant.setShort("lvl", (short) 20);
		getPlayer().setItemInHand(item.getItem());*/

		final Block block = Remain.getTargetBlock(getPlayer(), 5);
		final BlockState blockState = block.getState();

		final NBTTileEntity nbtTileEntity = new NBTTileEntity(blockState);

		//nbtTileEntity.setDouble("Paper.CookSpeedMultiplier", 1000D);
		//nbtTileEntity.setShort("BurnTime", Short.MAX_VALUE);

		if (blockState instanceof CreatureSpawner) {
			final CreatureSpawner spawner = (CreatureSpawner) blockState;

			spawner.setSpawnedType(EntityType.CHICKEN);
			blockState.update();

			tellSuccess("Creature spawner updated!");
		} else
			tellError("You must be looking at a spawner!");
	}
}
