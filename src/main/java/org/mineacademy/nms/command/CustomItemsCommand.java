package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.MinecraftVersion.V;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.model.SkullCreator;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompEnchantment;
import org.mineacademy.fo.remain.CompItemFlag;
import org.mineacademy.fo.remain.CompMaterial;

/**
 * A sample command to spawn customized items.
 */
public final class CustomItemsCommand extends NMSCommand {

	public CustomItemsCommand() {
		super("customitems");

		setDescription("Get custom items.");
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		// Way 1 : Custom Items

		final ItemCreator createdWool = ItemCreator.ofWool(CompColor.GREEN)
				.name("&aGreeeeeeny Wool")
				.enchant(CompEnchantment.DAMAGE_ALL, 5)
				.lore("Use this wool")
				.lore("item carefully!");

		final ItemCreator createdSword = ItemCreator.of(CompMaterial.DIAMOND_SWORD,
				"&bMythical Sword",
				"",
				"Use this sword",
				"with great care!")
				.enchant(CompEnchantment.DAMAGE_ALL, 5)
				.flags(CompItemFlag.HIDE_ATTRIBUTES);

		if (MinecraftVersion.atLeast(V.v1_13)) {
			//createdWool.enchant(BlackNovaEnchant.getInstance());
			//createdSword.enchant(BlackNovaEnchant.getInstance());
		}

		final ItemStack wool = createdWool.make();
		final ItemStack specialDiamondSword = createdSword.make();

		// Way 2 : Creating player skulls

		final ItemStack herobrine = ItemCreator.of(CompMaterial.PLAYER_HEAD)
				.skullOwner("Herobrine")
				.make();

		final ItemStack serious = ItemCreator.of(CompMaterial.PLAYER_HEAD)
				.skullOwner("Focus_YT9")
				.name("&fDO NOT LOOK").make();

		ItemStack tntItem = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/3af59776f2f03412c7b5947a63a0cf283d51fe65ac6df7f2f882e08344565e9");

		tntItem = ItemCreator.of(tntItem).name("&c&lTnT Item").make();

		getPlayer().getInventory().addItem(wool, specialDiamondSword, herobrine, serious, tntItem);
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}
