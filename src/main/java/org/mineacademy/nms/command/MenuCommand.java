package org.mineacademy.nms.command;

import org.mineacademy.nms.menu.StaffMenu;

/**
 * A sample command to open a menu we create in the course.
 */
public final class MenuCommand extends NMSCommand {

	public MenuCommand() {
		super("menu");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		new StaffMenu().displayTo(getPlayer());
	}
}
