package org.mineacademy.nms.command;

import java.util.List;

import org.mineacademy.fo.Common;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.nms.specific.entity.CustomEntity;

/**
 * A command spawning a custom NMS entity.
 */
public final class CustomEntityCommand extends NMSCommand {

	public CustomEntityCommand() {
		super("customentity");

		setMinArguments(1);
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();
		checkBoolean(MinecraftVersion.atLeast(MinecraftVersion.V.v1_8), "This command requires Minecraft 1.16+.");

		final CustomEntity entity = ReflectionUtil.lookupEnumSilent(CustomEntity.class, args[0].toUpperCase());
		checkNotNull(entity, "The entity you've entered, " + args[0] + ", is invalid. Available: " + Common.join(CustomEntity.values()));

		entity.spawn(getPlayer().getLocation());
	}

	@Override
	protected List<String> tabComplete() {
		return completeLastWord(CustomEntity.values());
	}
}
