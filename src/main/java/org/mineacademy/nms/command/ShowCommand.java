package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.entity.Player;
import org.mineacademy.fo.model.Variables;
import org.mineacademy.fo.remain.CompBarColor;
import org.mineacademy.fo.remain.CompBarStyle;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.model.Board;

/**
 * A sample command to show animated stuff.
 */
public final class ShowCommand extends NMSCommand {

	public ShowCommand() {
		super("show");

		setMinArguments(1);
		setDescription("Show different stuff.");
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();

		// /show title Hello world asda aads ads addas ads

		final String type = args[0];

		if ("board".equals(type)) {
			final Board board = Board.getInstance();

			if (board.isViewing(player))
				board.hide(player);
			else
				board.show(player);

			return;
		}

		checkBoolean(args.length > 1, "Please also set the message to show up.");
		final String message = joinArgs(1);

		if ("action".equals(type))
			Remain.sendActionBar(player, "#CCFF11" + Variables.replace(message, player));

		else if ("title".equals(type)) {
			final String[] split = message.split("\\|");
			final String title = split[0];
			final String subtitle = split.length == 1 ? "" : split[1];

			Remain.sendTitle(player, 20, 2 * 20, 0, title, subtitle);

		} else if ("boss".equals(type))
			Remain.sendBossbarTimed(player, message, 5, CompBarColor.GREEN, CompBarStyle.SOLID);
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord("action", "title", "boss", "board");

		return NO_COMPLETE;
	}
}
