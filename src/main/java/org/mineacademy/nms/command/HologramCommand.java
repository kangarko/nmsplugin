package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.nms.specific.hologram.NMSHologram;

/**
 * A sample command to open a menu we create in the course.
 */
public final class HologramCommand extends NMSCommand {

	public HologramCommand() {
		super("hologram");

		this.setMinArguments(2);
		this.setUsage("<param> <text... >");
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#onCommand()
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String param = args[0];
		final Player player = getPlayer();
		final Location location = player.getLocation();

		// /hologram show hello world this|is my hologram

		if ("show".equals(param)) {
			final String[] linesToShow = joinArgs(1).split("\\|");

			NMSHologram.sendTo(location, player, linesToShow);
		}
	}

	@Override
	protected List<String> tabComplete() {
		return this.args.length == 1 ? this.completeLastWord("show") : NO_COMPLETE;
	}
}
