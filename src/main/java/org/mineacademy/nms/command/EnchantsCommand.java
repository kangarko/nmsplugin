package org.mineacademy.nms.command;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.MinecraftVersion.V;
import org.mineacademy.nms.enchant.BlackNovaEnchant;
import org.mineacademy.nms.enchant.HideEnchant;

/**
 * A sample command to add custom enchants to the player with sample items.
 */
public final class EnchantsCommand extends NMSCommand {

	/**
	 * Create a new standalone command /sample
	 */
	public EnchantsCommand() {
		super("enchants");

		setDescription("Treat yourself to some gooood enchantments.");
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {
		checkConsole();
		checkBoolean(MinecraftVersion.atLeast(V.v1_8), "Custom enchants require at least Minecraft 1.13.");

		final ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
		item.addEnchantment(BlackNovaEnchant.getInstance().toBukkit(), 1);

		getPlayer().getInventory().addItem(item);

		final ItemStack bow = new ItemStack(Material.BOW);
		bow.addEnchantment(HideEnchant.getInstance().toBukkit(), 1);

		getPlayer().getInventory().addItem(bow);
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}