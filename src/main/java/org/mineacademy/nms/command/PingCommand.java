package org.mineacademy.nms.command;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.entity.Player;
import org.mineacademy.fo.ReflectionUtil;

/**
 * A sample command to show how to get a player's ping.
 */
public final class PingCommand extends NMSCommand {

	/**
	 * Create a new standalone command /sample
	 */
	public PingCommand() {
		super("ping");

		setDescription("Return your ping using Reflection");
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		// Way 1: Java native reflection.
		int ping = -1;

		final Player player = getPlayer();

		try {
			final Object entityPlayerInstance = player.getClass().getMethod("getHandle").invoke(player);
			final Field pingField = entityPlayerInstance.getClass().getField("ping");

			pingField.setAccessible(true);

			ping = (int) pingField.get(entityPlayerInstance);

		} catch (final ReflectiveOperationException ex) {
			returnTell("An error occured, please see the console.");

			ex.printStackTrace();
		}

		tell("Your ping is: " + ping + "ms");

		// Way 2: ReflectionUtil
		final Object entityPlayerInstance = ReflectionUtil.invoke("getHandle", player);
		ping = ReflectionUtil.getFieldContent(entityPlayerInstance, "ping");

		tell("Your ping (using Foundation) is: " + ping + "ms");

		// Way 3: Foundation
		//PlayerUtil.getPing(player);
	}

	/**
	 * @see SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}
