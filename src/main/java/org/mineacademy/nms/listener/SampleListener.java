package org.mineacademy.nms.listener;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.debug.Debugger;
import org.mineacademy.fo.debug.LagCatcher;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.PlayerCache;
import org.mineacademy.nms.citizens.QuestItems;
import org.mineacademy.nms.specific.entity.CustomEntity;
import org.mineacademy.nms.specific.goal.PathfinderGoalProvider;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * A sample listener for events.
 */
@AutoRegister
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SampleListener implements Listener {

	/**
	 * The singleton instance
	 */
	@Getter
	private static final SampleListener instance = new SampleListener();

	/**
	 * Listen for player join and loads his data
	 *
	 * @param event
	 */
	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		LagCatcher.start("join-event");

		final Player player = event.getPlayer();

		try {
			PlayerCache.from(player); // Load player's cache
			Messenger.success(player, "Welcome to the game!");

		} finally {
			LagCatcher.end("join-event", true);
		}

		CompParticle.FLAME.spawn(player.getLocation());
		//CompAttribute.GENERIC_FLYING_SPEED.set(player, 2);

		Remain.sendActionBar(player, "&cHey and thanks for joining!");

		Debugger.debug("player-listener", "Player " + player.getName() + " joins now");
	}

	@EventHandler
	public void onQuit(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();

		if (player.hasMetadata("CustomSign"))
			player.removeMetadata("CustomSign", SimplePlugin.getInstance());
	}

	@EventHandler
	public void onDeath(final PlayerDeathEvent event) {
		Remain.respawn(event.getEntity()); // 20 ticks = 1 second (1000 milliseconds, 1 tick = 50 milliseconds)
		// 20 ticks = 0.1 seconds because it's 50millis * 2 = 100ms = 0.1s
	}

	@EventHandler
	public void onEntityRightClick(final PlayerInteractAtEntityEvent event) {
		final Player player = event.getPlayer();
		final Entity rightClicked = event.getRightClicked();

		// Since 1.9 this event is fired TWICE, once for left hand, once for right hand
		// We are only interested in processing this from the main hand
		if (!Remain.isInteractEventPrimaryHand(event))
			return;

		if (!MinecraftVersion.atLeast(MinecraftVersion.V.v1_8))
			return;

		if (rightClicked instanceof Wolf) {

			if (CompMetadata.hasMetadata(rightClicked, "PetOwner")) {
				Messenger.error(player, "This wolf is already owned by " + CompMetadata.getMetadata(rightClicked, "PetOwner"));

				return;
			}

			final Wolf wolf = (Wolf) rightClicked;

			// Color it
			wolf.setCollarColor(CompColor.PINK.getDye());
			Remain.setCustomName(wolf, "&d" + player.getName() + "'s Wolf");

			// Apply behavior
			PathfinderGoalProvider.addPetNavigationTo(wolf);

			// Mark it
			CompMetadata.setMetadata(rightClicked, "PetOwner", player.getUniqueId().toString());

			// Notify new owner
			Messenger.success(player, "The wolf now belongs to you!");
		} else if (CompMetadata.hasMetadata(rightClicked, "CustomEntity"))
			if (rightClicked.getPassenger() == null)
				rightClicked.setPassenger(player);
	}

	/**
	 * Listen to when chunks are loaded (such as when discovered by player or upon reload)
	 * and reinject our custom mobs.
	 *
	 * @param event
	 */
	@EventHandler
	public void onChunkLoad(final ChunkLoadEvent event) {

		if (!MinecraftVersion.atLeast(MinecraftVersion.V.v1_16))
			return;

		for (final Entity entity : event.getChunk().getEntities())
			if (CompMetadata.hasMetadata(entity, "CustomEntity")) {
				final LivingEntity livingEntity = (LivingEntity) entity;

				final String entityName = CompMetadata.getMetadata(entity, "CustomEntity");
				final CustomEntity customEntity = CustomEntity.valueOf(entityName);

				final Location oldLocation = entity.getLocation().clone();
				final double oldHealth = livingEntity.getHealth();

				// Remove NMS snowman
				entity.remove();

				// Inject our own instead
				final LivingEntity transformed = customEntity.spawn(oldLocation);
				transformed.setHealth(oldHealth);
			}
	}

	@EventHandler
	public void onNPCDeath(final EntityDeathEvent event) {

		if (!HookManager.isCitizensLoaded())
			return;

		final LivingEntity entity = event.getEntity();

		// cat OR Boss cat?
		if (CompMetadata.hasMetadata(entity, "Citizens-Boss")) {
			final List<ItemStack> drops = event.getDrops();

			drops.clear();
			drops.add(QuestItems.getKillBossItem());
		}
	}

}
