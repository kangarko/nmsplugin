package org.mineacademy.nms;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.entity.Player;
import org.mineacademy.fo.constants.FoConstants;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.settings.YamlConfig;
import org.mineacademy.fo.visual.VisualizedRegion;

import lombok.Getter;
import lombok.Setter;

/**
 * A sample player cache storing permanent player information
 * to data.db or MySQL database for players.
 */
@Getter
public final class PlayerCache extends YamlConfig {

	/**
	 * The player cache map caching data for players online.
	 */
	private static final Map<UUID, PlayerCache> cacheMap = new HashMap<>();

	/**
	 * This instance's player's unique id
	 */
	private final UUID uniqueId;

	/**
	 * This instance's player's name
	 */
	private final String playerName;

	@Setter
	private boolean confirmedWorldScanCommand;

	private final VisualizedRegion region = new VisualizedRegion(); // < put primary and secondary location and then show it

	//
	// Store any custom saveable data here
	//

	/*
	 * Creates a new player cache (see the bottom)
	 */
	private PlayerCache(final String name, final UUID uniqueId) {
		this.playerName = name;
		this.uniqueId = uniqueId;

		this.setPathPrefix("Players." + uniqueId.toString());
		this.loadConfiguration(NO_DEFAULT, FoConstants.File.DATA);
	}

	/**
	 * Automatically called when loading data from disk.
	 *
	 * @see org.mineacademy.fo.settings.YamlConfig#onLoad()
	 */
	@Override
	protected void onLoad() {
		//
		// Load any custom fields here, example:
		// this.chatColor = get("Chat_Color", CompChatColor.class);
		//
	}

	@Override
	public void onSave() {
		//
		// Save any custom fields here, example:
		// this.set("Chat_Color", this.chatColor);
		//
	}

	/* ------------------------------------------------------------------------------- */
	/* Data-related methods */
	/* ------------------------------------------------------------------------------- */

	//
	// Implement your own data getter/setters here according to this example:
	//

	//public boolean hasChatColor() {
	//	return this.chatColor != null;
	//}

	//public void setChatColor(@Nullable CompChatColor chatColor) {
	//	this.chatColor = chatColor;
	//
	//	save();
	//}

	/* ------------------------------------------------------------------------------- */
	/* Misc methods */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Return player from cache if online or null otherwise
	 *
	 * @return
	 */
	@Nullable
	public Player toPlayer() {
		final Player player = Remain.getPlayerByUUID(this.uniqueId);

		return player != null && player.isOnline() ? player : null;
	}

	/**
	 * Remove this cached data from memory if it exists
	 */
	public void removeFromMemory() {
		cacheMap.remove(this.uniqueId);
	}

	@Override
	public String toString() {
		return "PlayerCache{" + this.playerName + ", " + this.uniqueId + "}";
	}

	/* ------------------------------------------------------------------------------- */
	/* Static access */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Return or create new player cache for the given player
	 *
	 * @param player
	 * @return
	 */
	public static PlayerCache from(final Player player) {
		final UUID uniqueId = player.getUniqueId();
		final String playerName = player.getName();

		PlayerCache cache = cacheMap.get(uniqueId);

		if (cache == null) {
			cache = new PlayerCache(playerName, uniqueId);

			cacheMap.put(uniqueId, cache);
		}

		return cache;
	}

	/**
	 * Clear the entire cache map
	 */
	public static void clearCaches() {
		cacheMap.clear();
	}
}
