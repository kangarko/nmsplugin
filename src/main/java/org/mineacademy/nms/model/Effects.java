package org.mineacademy.nms.model;

import org.mineacademy.fo.Valid;
import org.mineacademy.nms.NMSPlugin;

import de.slikey.effectlib.EffectManager;

/**
 * Represents a third-party hook for EffectsLib library.
 *
 * Used to soft-depend on the library without crashing the plugin
 * if the library is not installed on the server (as a plugin).
 */
public final class Effects {

	/**
	 * Our main effects manager
	 */
	private static EffectManager effectManager;

	/**
	 * Load the effects manager
	 */
	public static void load() {
		effectManager = new EffectManager(NMSPlugin.getInstance());
	}

	/**
	 * Unload the effects manager
	 */
	public static void disable() {
		effectManager.dispose();
	}

	/**
	 *
	 * @return
	 */
	public static EffectManager getEffectManager() {
		Valid.checkNotNull(effectManager, "Install EffectsLib as a plugin and then call Effects#load first before calling getEffectManager()");

		return effectManager;
	}
}
