package org.mineacademy.nms.model;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.debug.Debugger;
import org.mineacademy.fo.model.PacketListener;
import org.mineacademy.fo.plugin.SimplePlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedServerPing;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * A sample packet listener utilizing ProtocolLib
 */
@AutoRegister(hideIncompatibilityWarnings = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Packets extends PacketListener {

	@Getter
	private static final Packets instance = new Packets();

	/**
	 * Register and initiate packet listening
	 */
	@Override
	public void onRegister() {

		/*addSendingListener(PacketType.Play.Server.PLAYER_INFO, event -> {
			final PacketContainer packet = event.getPacket();
			final List<PlayerInfoData> playerInfo = packet.getPlayerInfoDataLists().read(0);
		
			for (int i = 0; i < playerInfo.size(); i++) {
				final PlayerInfoData data = playerInfo.get(i);
				final WrappedGameProfile profile = data.getProfile();
				final String oldName = profile.getName();
		
				if (oldName.equals("kangarko")) {
					final WrappedGameProfile copyProfile = new WrappedGameProfile(profile.getUUID(), "demoaccount");
					final PlayerInfoData copy = new PlayerInfoData(copyProfile, data.getLatency(), data.getGameMode(), data.getDisplayName()); // probably change this as well if used
		
					playerInfo.set(i, copy);
				}
			}
		
			packet.getPlayerInfoDataLists().write(0, playerInfo);
		});*/

		addReceivingListener(PacketType.Play.Client.UPDATE_SIGN, event -> {
			final Player player = event.getPlayer();
			final PacketContainer container = event.getPacket();

			if (player.hasMetadata("CustomSign")) {
				final WrappedChatComponent[] components = container.getChatComponentArrays().readSafely(0);

				if (components != null) {
					for (final WrappedChatComponent component : components)
						System.out.println(component.getJson());

				} else {
					final String[] lines = container.getStringArrays().read(0);

					// You can handle your logic of the plugin here
					Debugger.printValues(lines);
				}

				player.removeMetadata("CustomSign", SimplePlugin.getInstance());
			}
		});

		/*addSendingListener(PacketType.Play.Server.SPAWN_ENTITY_LIVING, event -> {
			final PacketContainer container = event.getPacket();

			final int oldId = container.getIntegers().read(1);

			System.out.println(oldId);

			if (oldId == 64)
				container.getIntegers().write(1, 65);
		});*/

		/*addPacketListener(new PacketAdapter(SimplePlugin.getInstance(), PacketType.values()) {
			@Override
			public void onPacketReceiving(final PacketEvent event) {
				//System.out.println("[READING] " + event.getPacket().getHandle());
			}

			@Override
			public void onPacketSending(final PacketEvent event) {
				//System.out.println("[WRITING] " + event.getPacket().getHandle());

				final PacketContainer packet = event.getPacket();

				//if (packet.getHandle() instanceof PacketPlayOutMapChunk) {
				//	event.setCancelled(true);
				//}

				//if (packet.getType() == PacketType.Play.Server.MAP_CHUNK) {
				//	event.setCancelled(true);
				//}

				if (packet.getType() == PacketType.Play.Server.UPDATE_TIME) {
					//final long oldTime = packet.getLongs().read(1);

					//System.out.println(oldTime);
					//packet.getLongs().write(1, 13_000L);
				}
			}
		});*/

		/*addSendingListener(PacketType.Play.Server.WORLD_PARTICLES, event -> {
			final Object handle = event.getPacket().getHandle();

			System.out.println(handle);
		});*/

		addSendingListener(PacketType.Status.Server.OUT_SERVER_INFO, event -> {

			final WrappedServerPing ping = event.getPacket().getServerPings().read(0);

			ping.setMotD(Common.colorize("&cHacked server list menu!"));
			//ping.setVersionName("Paper 1.20-alpha");
			//ping.setVersionProtocol(-666);

			ping.setPlayersOnline(20000);
			ping.setPlayersMaximum(20001);

			ping.setPlayers(compileHoverText(
					"First line text here",
					"",
					"third &7line!"));
		});

		//
		// Implement your own packet listeners here, samples below:
		//

		// Receiving tab complete
		/*addReceivingListener(ListenerPriority.HIGHEST, PacketType.Play.Client.TAB_COMPLETE, event -> {
			final String buffer = event.getPacket().getStrings().read(0);

			Common.log("Received tab complete packet '" + buffer + "' to " + event.getPlayer().getName());
		});*/

		// A custom handler for sending chat messages (they are pretty complicated to decipher
		// so we made a wrapper for you)
		/*addPacketListener(new PacketUtil.SimpleChatAdapter() {

			@Override
			protected String onMessage(final String message) {
				Common.log("Sending chat packet '" + message + "' to " + this.getPlayer().getName());

				// If you want to prevent this message from being shown to player cancel the event here:
				//getEvent().setCancelled(true);

				//if (message.contains("attribute"))
				//	getEvent().setCancelled(true);

				//message = "hello";

				return message;
			}

			@Override
			protected void onJsonMessage(final String jsonMessage) {
				System.out.println("JSON message is: " + jsonMessage);
			}
		});*/
	}

}
