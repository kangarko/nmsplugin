package org.mineacademy.nms.model;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.model.OfflineRegionScanner;
import org.mineacademy.fo.remain.CompMaterial;

public class RegionScanner extends OfflineRegionScanner {

	@Override
	protected void onChunkScan(final Chunk chunk) {
		/*for (final Entity entity : chunk.getEntities()) {
			if (entity instanceof TNTPrimed)
				entity.remove();

			if (entity.getType() == EntityType.ARROW)
				entity.remove();
		}*/

		/*for (BlockState tileEntity : chunk.getTileEntities()) {
			if (tileEntity instanceof Hopper) {
				Hopper hopper = (Hopper) tileEntity;
				//hopper.getInventory().setItem(0, );

				tileEntity.update();
			}

			// org.bukkit.block.Sign
		}*/

		// Winter plugin recreation - will cover the world with snow and freeze water
		for (final Block block : this.findBlocksToCover(chunk)) {
			final Block ground = block.getRelative(BlockFace.DOWN);

			if (block.getType() != CompMaterial.SNOW.getMaterial())
				block.setType(CompMaterial.SNOW.getMaterial());

			if (ground.getType().toString().contains("WATER"))
				ground.setType(CompMaterial.ICE.getMaterial());

			// Reverse the above behavior
			/*if (block.getType() == CompMaterial.SNOW.getMaterial())
				block.setType(CompMaterial.AIR.getMaterial());
			
			if (ground.getType() == CompMaterial.ICE.getMaterial())
				ground.setType(CompMaterial.WATER.getMaterial());*/
		}
	}

	private List<Block> findBlocksToCover(final Chunk chunk) {
		final List<Block> locations = new ArrayList<>();

		for (final Location location : BlockUtil.getXZLocations(chunk)) {
			final int posY = BlockUtil.findHighestBlockNoSnow(location);
			location.setY(posY);

			locations.add(location.getBlock());
		}

		return locations;
	}

	@Override
	protected void onScanFinished() {

	}
}
