package org.mineacademy.nms.model;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.visual.VisualTool;
import org.mineacademy.fo.visual.VisualizedRegion;
import org.mineacademy.nms.PlayerCache;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * An automatically registered tool you can use in the game
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RegionTool extends VisualTool {

	/**
	 * The singular tool instance
	 */
	@Getter
	private static final Tool instance = new RegionTool();

	/**
	 * The actual item stored here for maximum performance
	 */
	private ItemStack item;

	/**
	 * @see Tool#getItem()
	 */
	@Override
	public ItemStack getItem() {

		if (item == null)
			item = ItemCreator.of(
					CompMaterial.IRON_AXE,
					"Region Tool",
					"",
					"Click blocks to",
					"select a region.")
					.make();

		return item;
	}

	/**
	 * Cancel the event so that we don't destroy blocks when selecting them
	 *
	 * @see Tool#autoCancel()
	 */
	@Override
	protected boolean autoCancel() {
		return true;
	}

	@Override
	protected void handleBlockClick(final Player player, final ClickType click, final Block block) {
		final PlayerCache cache = PlayerCache.from(player);
		final Location clickedLocation = block.getLocation();

		if (click == ClickType.RIGHT) {
			cache.getRegion().setSecondary(clickedLocation);

			Messenger.success(player, "Secondary location set.");
		} else if (click == ClickType.LEFT) {
			cache.getRegion().setPrimary(clickedLocation);

			Messenger.success(player, "Primary location set.");
		}
	}

	@Override
	protected List<Location> getVisualizedPoints(final Player player) {
		final VisualizedRegion region = PlayerCache.from(player).getRegion();
		final List<Location> points = new ArrayList<>();

		if (region.getPrimary() != null)
			points.add(region.getPrimary());

		if (region.getSecondary() != null)
			points.add(region.getSecondary());

		return points;
	}

	@Override
	protected VisualizedRegion getVisualizedRegion(final Player player) {

		return PlayerCache.from(player).getRegion();
	}

	@Override
	protected String getBlockName(final Block block, final Player player) {
		return "Region Point";
	}

	@Override
	protected CompMaterial getBlockMask(final Block block, final Player player) {
		return CompMaterial.EMERALD_BLOCK;
	}
}
