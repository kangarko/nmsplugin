package org.mineacademy.nms.model;

import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.model.Replacer;
import org.mineacademy.fo.model.SimpleScoreboard;
import org.mineacademy.fo.remain.Remain;

import lombok.Getter;

/**
 * A sample scoreboard
 */
public class Board extends SimpleScoreboard {

	/**
	 * The main instance of this scoreboard
	 */
	@Getter
	private static final Board instance = new Board();

	/*
	 * Create and setup a new scoreboard
	 */
	private Board() {
		this.setUpdateDelayTicks(10);

		this.setTitle("&8[ &6Demo Board &8]");

		this.addRows(
				"",
				"&7&l> &c&lPlayer Info:",
				"&7Ping: &f{ping}",
				"",
				"&7&l> &c&lHealth:",
				"&7{health}",
				"",
				"&7&l> &c&lTime:",
				"&7{time}",
				"",
				"&7&l> &c&lServer Info:",
				"&f{tps} &7TPS");
	}

	@Override
	protected void onUpdate() {
		// edit rows here
	}

	@Override
	protected String replaceVariables(final Player player, final String message) {

		return Replacer.replaceArray(message,
				"ping", PlayerUtil.getPing(player),
				"health", player.getHealth(),
				"tps", Remain.getTPS(),
				"time", TimeUtil.getFormattedDate());
	}
}
