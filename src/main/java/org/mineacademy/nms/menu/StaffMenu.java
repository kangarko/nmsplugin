package org.mineacademy.nms.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.MenuPagged;
import org.mineacademy.fo.menu.MenuQuantitable;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.ButtonMenu;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.model.MenuQuantity;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMonsterEgg;
import org.mineacademy.fo.remain.Remain;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents a sample menu we create in the training.
 */
public class StaffMenu extends Menu {

	private final Button selectPlayerButton;
	private final Button selectMobButton;

	/**
	 *
	 */
	public StaffMenu() {
		// Return Back Button
		// Info Button
		// The Hotbar
		// The Menu. static methods

		// PagedMenu
		// Quantitable Menu

		setTitle("Staff Menu");
		setSize(9 * 4);

		//setSlotNumbersVisible();

		this.selectPlayerButton = new ButtonMenu(new PlayerSelectionMenu(this), ItemCreator.of(CompMaterial.PLAYER_HEAD,
				"Select A Dude",
				"",
				"Click This Button",
				"To Open A Dude",
				"Selection Menu."));

		this.selectMobButton = new ButtonMenu(new MobSelectionMenu(this), ItemCreator.of(
				CompMaterial.SPAWNER,
				"Get Eggs",
				"",
				"Click This To",
				"Obtain Mob Eggs!"));
	}

	/**
	 * @see org.mineacademy.fo.menu.Menu#getItemAt(int)
	 */
	@Override
	public ItemStack getItemAt(int slot) {

		if (slot == 9 + 3)
			return this.selectPlayerButton.getItem();

		if (slot == 9 + 5)
			return this.selectMobButton.getItem();

		return null;
	}

	/**
	 * @see org.mineacademy.fo.menu.Menu#getInfo()
	 */
	@Override
	protected String[] getInfo() {
		return new String[]{
				"Use this menu with care because",
				"it enables you to deal with",
				"every online dude on your server."
		};
	}

	private final class MobSelectionMenu extends MenuPagged<EntityType> {

		/**
		 * @param parent
		 */
		protected MobSelectionMenu(Menu parent) {

			super(parent, Arrays.asList(EntityType.values())
					.stream()
					.filter(entity -> entity.isAlive() && entity.isSpawnable())
					.collect(Collectors.toList()));

		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#convertToItemStack(java.lang.Object)
		 */
		@Override
		protected ItemStack convertToItemStack(EntityType item) {
			return CompMonsterEgg.makeEgg(item);
		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#onPageClick(org.bukkit.entity.Player, java.lang.Object, org.bukkit.event.inventory.ClickType)
		 */
		@Override
		protected void onPageClick(Player player, EntityType item, ClickType click) {
			player.getInventory().addItem(CompMonsterEgg.makeEgg(item));
		}
	}

	private final class PlayerSelectionMenu extends MenuPagged<Player> {

		/**
		 *
		 */
		private PlayerSelectionMenu(Menu parent) {
			super(parent, compilePlayers());
		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#convertToItemStack(java.lang.Object)
		 */
		@Override
		protected ItemStack convertToItemStack(Player item) {

			return ItemCreator.of(CompMaterial.PLAYER_HEAD,
							item.getName(),
							"",
							"Click To Open",
							"A Menu For " + item.getName())
					.skullOwner(item.getName())
					.make();
		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#onPageClick(org.bukkit.entity.Player, java.lang.Object, org.bukkit.event.inventory.ClickType)
		 */
		@Override
		protected void onPageClick(Player playerWhosViewingTheMenu, Player selectedPlayer, ClickType click) {
			new PotionSelectionMenu(selectedPlayer, this).displayTo(playerWhosViewingTheMenu);
		}
	}

	private static List<Player> compilePlayers() {
		final List<Player> list = new ArrayList<>();

		for (final Player onlinePlayer : Remain.getOnlinePlayers())
			list.add(onlinePlayer);

		return list;
	}

	private final class PotionSelectionMenu extends MenuPagged<PotionEffectType> implements MenuQuantitable {

		@Getter
		@Setter
		private MenuQuantity quantity = MenuQuantity.ONE;

		private final Player selectedPlayer;

		protected PotionSelectionMenu(Player selectedPlayer, Menu parent) {
			super(parent, Arrays.asList(PotionEffectType.values()));

			this.selectedPlayer = selectedPlayer;
		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#convertToItemStack(java.lang.Object)
		 */
		@Override
		protected ItemStack convertToItemStack(PotionEffectType type) {

			final int level = this.getPotionLevel(type);

			final ItemStack item = ItemCreator.of(CompMaterial.POTION,
							"Change your " + ItemUtil.bountifyCapitalized(type.getName()),
							"",
							"Current: " + level,
							"",
							"&8(&7Mouse click&8)",
							"< -{q} +{q} >".replace("{q}", quantity.getAmountPercent() + ""))
					.amount(level == 0 ? 1 : level)
					.make();

			Remain.setPotion(item, type, level);

			return item;
		}

		/**
		 * @see org.mineacademy.fo.menu.MenuPagged#onPageClick(org.bukkit.entity.Player, java.lang.Object, org.bukkit.event.inventory.ClickType)
		 */
		@Override
		protected void onPageClick(Player currentPlayer, PotionEffectType type, ClickType click) {

			final int oldLevel = this.getPotionLevel(type);
			final int newLevel = (int) MathUtil.range(oldLevel + this.getNextQuantityPercent(click), 0, 10);

			if (newLevel == 0)
				this.selectedPlayer.removePotionEffect(type);
			else
				this.selectedPlayer.addPotionEffect(new PotionEffect(type, Integer.MAX_VALUE, newLevel - 1), true);

			this.restartMenu("Set " + ItemUtil.bountifyCapitalized(type.getName()) + " to lvl " + newLevel);
		}

		/**
		 * @see org.mineacademy.fo.menu.Menu#newInstance()
		 */
		@Override
		public Menu newInstance() {
			return new PotionSelectionMenu(this.selectedPlayer, this.getParent());
		}

		private int getPotionLevel(PotionEffectType type) {

			for (final PotionEffect effect : this.selectedPlayer.getActivePotionEffects())
				if (effect.getType().equals(type))
					return effect.getAmplifier() + 1; // level 1 when amplifier is 0

			return 0;
		}
	}
}
