// TODO Update to Minecraft 1.21
/*package org.mineacademy.nms.specific.enchant;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_20_R4.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_20_R4.util.CraftNamespacedKey;
import org.mineacademy.fo.enchant.NmsEnchant;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.remain.CompEquipmentSlot;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantment.EnchantmentDefinition;

public class CustomEnchant_v1_20_5 implements NmsEnchant {

	private final EnchantmentDefinition definition;
	private final SimpleEnchantment simpleEnchantment;
	private final Enchantment enchantment;

	protected CustomEnchant_v1_20_5(SimpleEnchantment wrapper) {

		// TODO Fix players being kicked out with invalid set slot packet
		if (true)
			throw new RuntimeException("Not implemented yet");

		this.simpleEnchantment = wrapper;

		List<net.minecraft.world.entity.EquipmentSlot> nmsSlots = new ArrayList<>();

		for (CompEquipmentSlot slot : wrapper.getActiveSlots()) {
			EquipmentSlot nmsSlot = null;

			if (slot == CompEquipmentSlot.HAND)
				nmsSlot = EquipmentSlot.MAINHAND;

			if (nmsSlot == null)
				try {
					nmsSlot = EquipmentSlot.valueOf(slot.getKey());
				} catch (IllegalArgumentException ex) {
				}

			if (nmsSlot == null)
				try {
					nmsSlot = EquipmentSlot.valueOf(slot.getKey().replace("_", ""));
				} catch (IllegalArgumentException ex) {
				}

			if (nmsSlot == null)
				try {
					nmsSlot = EquipmentSlot.valueOf(slot.getBukkitName());
				} catch (IllegalArgumentException ex) {
				}

			if (nmsSlot != null)
				nmsSlots.add(nmsSlot);
		}

		final SimpleEnchantment.Cost minCost = wrapper.getMinCost();
		final SimpleEnchantment.Cost maxCost = wrapper.getMaxCost();

		this.definition = Enchantment.definition(
				ItemTags.FIRE_ASPECT_ENCHANTABLE,
				2,
				wrapper.getMaxLevel(),
				new Enchantment.Cost(minCost.getBase(), minCost.getPerLevel()),
				new Enchantment.Cost(maxCost.getBase(), maxCost.getPerLevel()),
				wrapper.getAnvilCost(),
				nmsSlots.toArray(new EquipmentSlot[nmsSlots.size()]));

		this.enchantment = new Enchantment(this.definition) {

			@Override
			protected boolean checkCompatibility(Enchantment other) {
				final ResourceLocation location = BuiltInRegistries.ENCHANTMENT.getKey(other);

				if (location == null)
					return false;

				final NamespacedKey key = CraftNamespacedKey.fromMinecraft(location);
				final org.bukkit.enchantments.Enchantment enchantment = org.bukkit.Registry.ENCHANTMENT.get(key);

				if (enchantment == null)
					return false;

				return !wrapper.conflictsWith(enchantment);
			}

			@Override
			public boolean canEnchant(ItemStack item) {
				return wrapper.canEnchantItem(CraftItemStack.asBukkitCopy(item)) || super.canEnchant(item);
			}

			@Override
			public boolean isTreasureOnly() {
				return wrapper.isTreasure() || this.isCurse();
			}

			@Override
			public boolean isCurse() {
				return wrapper.isCursed();
			}

			@Override
			public boolean isTradeable() {
				return wrapper.isTradeable();
			}

			@Override
			public boolean isDiscoverable() {
				return wrapper.isDiscoverable();
			}
		};
	}

	@Override
	public void register() {
		Registry.register(BuiltInRegistries.ENCHANTMENT, this.simpleEnchantment.getNamespacedName(), this.enchantment);
	}

	@Override
	public org.bukkit.enchantments.Enchantment toBukkit() {
		return org.bukkit.enchantments.Enchantment.getByKey(
				new NamespacedKey("minecraft", this.simpleEnchantment.getNamespacedName()));
	}
}*/
