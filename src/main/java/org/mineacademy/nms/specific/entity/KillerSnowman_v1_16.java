package org.mineacademy.nms.specific.entity;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.remain.CompMetadata;

import net.minecraft.server.v1_16_R3.AttributeModifier;
import net.minecraft.server.v1_16_R3.BlockPosition;
import net.minecraft.server.v1_16_R3.DamageSource;
import net.minecraft.server.v1_16_R3.EntityChicken;
import net.minecraft.server.v1_16_R3.EntityHuman;
import net.minecraft.server.v1_16_R3.EntityLiving;
import net.minecraft.server.v1_16_R3.EntityPositionTypes;
import net.minecraft.server.v1_16_R3.EntitySnowball;
import net.minecraft.server.v1_16_R3.EntitySnowman;
import net.minecraft.server.v1_16_R3.EntityTypes;
import net.minecraft.server.v1_16_R3.EnumDifficulty;
import net.minecraft.server.v1_16_R3.EnumMobSpawn;
import net.minecraft.server.v1_16_R3.EnumMoveType;
import net.minecraft.server.v1_16_R3.GameRules;
import net.minecraft.server.v1_16_R3.GenericAttributes;
import net.minecraft.server.v1_16_R3.GroupDataEntity;
import net.minecraft.server.v1_16_R3.MathHelper;
import net.minecraft.server.v1_16_R3.NBTTagCompound;
import net.minecraft.server.v1_16_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_16_R3.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_16_R3.SoundEffect;
import net.minecraft.server.v1_16_R3.SoundEffects;
import net.minecraft.server.v1_16_R3.SpawnerCreature;
import net.minecraft.server.v1_16_R3.TagsFluid;
import net.minecraft.server.v1_16_R3.Vec3D;
import net.minecraft.server.v1_16_R3.WorldServer;

/**
 * Represents a customized snowman entity.
 */
final class KillerSnowman_v1_16 extends EntitySnowman implements NMSEntity {

	/**
	 * Create a new snowman at the given location.
	 * <p>
	 * **You must call this using {@link CustomEntity} class for it to be
	 * properly registered!**
	 *
	 * @param location
	 */
	public KillerSnowman_v1_16(final Location location) {
		super(EntityTypes.SNOW_GOLEM, ((CraftWorld) location.getWorld()).getHandle());

		// How entity moves
		this.goalSelector.a(1, new PathfinderGoalMeleeAttack(this, 10, true));

		// How entity targets other entities
		this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget<>(this, EntityHuman.class, true));

		// Remove the pumpkin
		this.setHasPumpkin(false);

		// Despawn flag
		this.persistent = false;

		// You can also get the Bukkit entity at this point and manipulate it
		final Entity bukkitEntity = getBukkitEntity();

		bukkitEntity.setCustomName("Killer Snowman");
		bukkitEntity.setCustomNameVisible(true);

		// Inject invisible flag that Bukkit recognizes and saves to make this mob
		// persistent over restarts
		CompMetadata.setMetadata(bukkitEntity, "CustomEntity", CustomEntity.SNOWMAN.toString());

		this.getAttributeMap().registerAttribute(GenericAttributes.SPAWN_REINFORCEMENTS);
	}

	/**
	 * Called automatically when this entity is hit
	 *
	 * @see net.minecraft.server.v1_16_R3.EntityLiving#damageEntity(net.minecraft.server.v1_16_R3.DamageSource, float)
	 */
	@Override
	public boolean damageEntity(final DamageSource source, final float damagePower) {

		if (!super.damageEntity(source, damagePower))
			return false;
		else if (!(this.world instanceof WorldServer))
			return false;
		else {
			final WorldServer worldserver = (WorldServer) this.world;
			EntityLiving entityliving = this.getGoalTarget();

			if (entityliving == null && source.getEntity() instanceof EntityLiving)
				entityliving = (EntityLiving) source.getEntity();

			// Spawns Zombie reinforcements when we damage this entity
			if (entityliving != null && this.world.getDifficulty() == EnumDifficulty.HARD && this.random.nextFloat() < this.b(GenericAttributes.SPAWN_REINFORCEMENTS) && this.world.getGameRules().getBoolean(GameRules.DO_MOB_SPAWNING)) {
				final int i = MathHelper.floor(this.locX());
				final int j = MathHelper.floor(this.locY());
				final int k = MathHelper.floor(this.locZ());
				final EntityChicken entityzombie = new EntityChicken(EntityTypes.CHICKEN, this.world);

				for (int l = 0; l < 50; ++l) {
					final int randomX = i + MathHelper.nextInt(this.random, 7, 40) * MathHelper.nextInt(this.random, -1, 1);
					final int randomY = j + MathHelper.nextInt(this.random, 7, 40) * MathHelper.nextInt(this.random, -1, 1);
					final int randomZ = k + MathHelper.nextInt(this.random, 7, 40) * MathHelper.nextInt(this.random, -1, 1);

					final BlockPosition position = new BlockPosition(randomX, randomY, randomZ);
					final EntityTypes<?> entitytypes = entityzombie.getEntityType();
					final EntityPositionTypes.Surface entitypositiontypes_surface = EntityPositionTypes.a(entitytypes);

					if (SpawnerCreature.a(entitypositiontypes_surface, this.world, position, entitytypes) && EntityPositionTypes.a(entitytypes, worldserver, EnumMobSpawn.REINFORCEMENT, position, this.world.random)) {
						entityzombie.setPosition(randomX, randomY, randomZ);
						if (!this.world.isPlayerNearby(randomX, randomY, randomZ, 7.0D) && this.world.j(entityzombie) && this.world.getCubes(entityzombie) && !this.world.containsLiquid(entityzombie.getBoundingBox())) {
							entityzombie.setGoalTarget(entityliving, EntityTargetEvent.TargetReason.REINFORCEMENT_TARGET, true);
							entityzombie.prepare(worldserver, this.world.getDamageScaler(entityzombie.getChunkCoordinates()), EnumMobSpawn.REINFORCEMENT, (GroupDataEntity) null, (NBTTagCompound) null);

							worldserver.addAllEntities(entityzombie, CreatureSpawnEvent.SpawnReason.REINFORCEMENTS);

							this.getAttributeInstance(GenericAttributes.SPAWN_REINFORCEMENTS).addModifier(new AttributeModifier("Zombie reinforcement caller charge", -0.05000000074505806D, AttributeModifier.Operation.ADDITION));
							entityzombie.getAttributeInstance(GenericAttributes.SPAWN_REINFORCEMENTS).addModifier(new AttributeModifier("Zombie reinforcement callee charge", -0.05000000074505806D, AttributeModifier.Operation.ADDITION));

							break;
						}
					}
				}
			}

			return true;
		}
	}

	/**
	 * Handles this snowmen shooting snowballs
	 *
	 * @see net.minecraft.server.v1_16_R3.EntitySnowman#a(net.minecraft.server.v1_16_R3.EntityLiving, float)
	 */
	@Override
	public void a(final EntityLiving entityliving, final float f) {
		final EntitySnowball snowball = new EntitySnowball(this.world, this);

		final double posX = entityliving.locX() - this.locX();
		final double posY = (entityliving.locY() + entityliving.getHeadHeight() - 1.100000023841858) - snowball.locY();
		final double posZ = entityliving.locZ() - this.locZ();
		final float f2 = MathHelper.sqrt(posX * posX + posZ * posZ) * 0.2f;

		snowball.shoot(posX, posY + f2, posZ, 1.6f, 12.0f);

		// Customize the code here to customize shooting sound
		this.playSound(SoundEffects.ENTITY_SHULKER_SHOOT, 1.0f, 1.0f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
		this.world.addEntity(snowball);

		// We inject our snowball tracking to damage entities which are hit
		EntityUtil.trackHit((Projectile) snowball.getBukkitEntity(), hitEvent -> {
			final Entity hitEntity = hitEvent.getHitEntity();

			if (hitEntity instanceof LivingEntity) {
				final LivingEntity living = (LivingEntity) hitEntity;

				living.setHealth(MathUtil.range(living.getHealth() - 4, 0, living.getHealth()));
			}
		});

	}

	@Override
	public void g(final Vec3D vec3d) {

		final double motionSpeed = 0.2;
		final double speed = 1.0;
		final double jumpVelocity = 0.7;

		// Do not call for dead entities
		if (!this.isAlive())
			return;

		// If there's not passenger, we'll default to NMS behavior
		if (!this.isVehicle()) {
			this.aE = 0.02F;
			super.g(vec3d);

			return;
		}

		final EntityLiving passenger = (EntityLiving) this.getPassengers().get(0);

		// Reset fall distance when back on ground
		if (this.onGround)
			this.fallDistance = 0;

		// Float up, when in water
		if (this.a(TagsFluid.WATER))
			this.setMot(this.getMot().add(0, 0.5, 0));

		// Make this mob look where the rider is looking
		this.setYawPitch(passenger.yaw, passenger.pitch * 0.5F);

		this.as = this.yaw;
		this.aW = this.yaw;

		final double sideMotion = passenger.aR * motionSpeed;
		double forwardMotion = passenger.aT * motionSpeed;

		// If we are going backwards, make backwards walk slower
		if (forwardMotion <= 0F)
			forwardMotion *= 0.25F;

		this.ride(sideMotion, forwardMotion, vec3d.y, (float) speed);

		// Handle jumping
		if (passenger.jumping && this.onGround) // prevent infinite jump
			this.setMot(this.getMot().x, jumpVelocity, this.getMot().z);

		super.a(vec3d);
	}

	// Inspired from pig, horse classes and
	// https://www.spigotmc.org/threads/1-15-entity-wasd-control.431302/
	// Updated and reworked for Minecraft 1.16 by MineAcademy
	private void ride(final double motionSideways, final double motionForward, final double motionUpwards, final float speedModifier) {

		double locY;
		float f2;
		final float speed;
		final float swimSpeed;

		// Behavior in water
		if (this.a(TagsFluid.WATER)) {
			locY = this.locY();
			speed = 0.8F;
			swimSpeed = 0.02F;

			this.a(swimSpeed, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.SELF, this.getMot());

			final double motX = this.getMot().x * speed;
			double motY = this.getMot().y * 0.800000011920929D;
			final double motZ = this.getMot().z * speed;
			motY -= 0.02D;

			if (this.positionChanged && this.e(this.getMot().x, this.getMot().y + 0.6000000238418579D - this.locY() + locY, this.getMot().z))
				motY = 0.30000001192092896D;

			this.setMot(motX, motY, motZ);

			// Behavior in lava
		} else if (this.a(TagsFluid.LAVA)) {
			locY = this.locY();

			this.a(2F, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.SELF, this.getMot());

			final double motX = this.getMot().x * 0.5D;
			double motY = this.getMot().y * 0.5D;
			final double motZ = this.getMot().z * 0.5D;

			motY -= 0.02D;

			if (this.positionChanged && this.e(this.getMot().x, this.getMot().y + 0.6000000238418579D - this.locY() + locY, this.getMot().z))
				motY = 0.30000001192092896D;

			this.setMot(motX, motY, motZ);

			// Behavior on land
		} else {
			final float friction = 0.91F;

			speed = speedModifier * (0.16277136F / (friction * friction * friction));

			this.a(speed, new Vec3D(motionSideways, motionUpwards, motionForward));

			double motX = this.getMot().x;
			double motY = this.getMot().y;
			double motZ = this.getMot().z;

			if (this.isClimbing()) {
				swimSpeed = 0.15F;
				motX = MathHelper.a(motX, -swimSpeed, swimSpeed);
				motZ = MathHelper.a(motZ, -swimSpeed, swimSpeed);
				this.fallDistance = 0.0F;

				if (motY < -0.15D)
					motY = -0.15D;
			}

			final Vec3D mot = new Vec3D(motX, motY, motZ);

			this.move(EnumMoveType.SELF, mot);

			if (this.positionChanged && this.isClimbing())
				motY = 0.2D;

			motY -= 0.08D;

			motY *= 0.9800000190734863D;
			motX *= friction;
			motZ *= friction;

			this.setMot(motX, motY, motZ);
		}

		this.as = this.aD;
		locY = this.locX() - this.lastX;
		final double d1 = this.locZ() - this.lastZ;
		f2 = MathHelper.sqrt(locY * locY + d1 * d1) * 4.0F;

		if (f2 > 1.0F)
			f2 = 1.0F;

		this.aD += (f2 - this.aD) * 0.4F;
		this.aE += this.aD;
	}

	@Override
	public LivingEntity spawn(final Location location) {
		this.setPosition(location.getX(), location.getY(), location.getZ());
		this.getWorld().addEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);

		return this.getBukkitLivingEntity();
	}

	/**
	 * Override this method to customize falling/step sound
	 */
	@Override
	protected SoundEffect getSoundFall(final int fallHeight) {
		return SoundEffects.ENTITY_WITHER_SKELETON_STEP;
	}

	/**
	 * Override this method to customize ambient/silent sound
	 */
	@Override
	protected SoundEffect getSoundAmbient() {
		return SoundEffects.ENTITY_WITCH_AMBIENT; // laughter
	}

	/**
	 * Override this method to customize death sound
	 */
	@Override
	protected SoundEffect getSoundDeath() {
		return SoundEffects.ENTITY_WITHER_SKELETON_DEATH;
	}
}
