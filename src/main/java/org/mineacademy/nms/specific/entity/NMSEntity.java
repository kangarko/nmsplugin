package org.mineacademy.nms.specific.entity;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

public interface NMSEntity {

	LivingEntity spawn(Location location);
}
