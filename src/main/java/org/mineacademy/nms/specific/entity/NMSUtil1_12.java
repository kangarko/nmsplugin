package org.mineacademy.nms.specific.entity;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_12_R1.BiomeBase;
import net.minecraft.server.v1_12_R1.BiomeBase.BiomeMeta;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EntityAreaEffectCloud;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EntityArrow;
import net.minecraft.server.v1_12_R1.EntityBat;
import net.minecraft.server.v1_12_R1.EntityBlaze;
import net.minecraft.server.v1_12_R1.EntityBoat;
import net.minecraft.server.v1_12_R1.EntityCaveSpider;
import net.minecraft.server.v1_12_R1.EntityChicken;
import net.minecraft.server.v1_12_R1.EntityCow;
import net.minecraft.server.v1_12_R1.EntityCreeper;
import net.minecraft.server.v1_12_R1.EntityDragonFireball;
import net.minecraft.server.v1_12_R1.EntityEgg;
import net.minecraft.server.v1_12_R1.EntityEnderCrystal;
import net.minecraft.server.v1_12_R1.EntityEnderDragon;
import net.minecraft.server.v1_12_R1.EntityEnderPearl;
import net.minecraft.server.v1_12_R1.EntityEnderSignal;
import net.minecraft.server.v1_12_R1.EntityEnderman;
import net.minecraft.server.v1_12_R1.EntityEndermite;
import net.minecraft.server.v1_12_R1.EntityEvoker;
import net.minecraft.server.v1_12_R1.EntityEvokerFangs;
import net.minecraft.server.v1_12_R1.EntityExperienceOrb;
import net.minecraft.server.v1_12_R1.EntityFallingBlock;
import net.minecraft.server.v1_12_R1.EntityFireball;
import net.minecraft.server.v1_12_R1.EntityFireworks;
import net.minecraft.server.v1_12_R1.EntityGhast;
import net.minecraft.server.v1_12_R1.EntityGiantZombie;
import net.minecraft.server.v1_12_R1.EntityGuardian;
import net.minecraft.server.v1_12_R1.EntityGuardianElder;
import net.minecraft.server.v1_12_R1.EntityHorse;
import net.minecraft.server.v1_12_R1.EntityHorseDonkey;
import net.minecraft.server.v1_12_R1.EntityHorseMule;
import net.minecraft.server.v1_12_R1.EntityHorseSkeleton;
import net.minecraft.server.v1_12_R1.EntityHorseZombie;
import net.minecraft.server.v1_12_R1.EntityIllagerIllusioner;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.EntityIronGolem;
import net.minecraft.server.v1_12_R1.EntityItem;
import net.minecraft.server.v1_12_R1.EntityItemFrame;
import net.minecraft.server.v1_12_R1.EntityLeash;
import net.minecraft.server.v1_12_R1.EntityLlama;
import net.minecraft.server.v1_12_R1.EntityLlamaSpit;
import net.minecraft.server.v1_12_R1.EntityMagmaCube;
import net.minecraft.server.v1_12_R1.EntityMinecartChest;
import net.minecraft.server.v1_12_R1.EntityMinecartCommandBlock;
import net.minecraft.server.v1_12_R1.EntityMinecartFurnace;
import net.minecraft.server.v1_12_R1.EntityMinecartHopper;
import net.minecraft.server.v1_12_R1.EntityMinecartMobSpawner;
import net.minecraft.server.v1_12_R1.EntityMinecartRideable;
import net.minecraft.server.v1_12_R1.EntityMinecartTNT;
import net.minecraft.server.v1_12_R1.EntityMushroomCow;
import net.minecraft.server.v1_12_R1.EntityOcelot;
import net.minecraft.server.v1_12_R1.EntityPainting;
import net.minecraft.server.v1_12_R1.EntityParrot;
import net.minecraft.server.v1_12_R1.EntityPig;
import net.minecraft.server.v1_12_R1.EntityPigZombie;
import net.minecraft.server.v1_12_R1.EntityPolarBear;
import net.minecraft.server.v1_12_R1.EntityPotion;
import net.minecraft.server.v1_12_R1.EntityRabbit;
import net.minecraft.server.v1_12_R1.EntitySheep;
import net.minecraft.server.v1_12_R1.EntityShulker;
import net.minecraft.server.v1_12_R1.EntityShulkerBullet;
import net.minecraft.server.v1_12_R1.EntitySilverfish;
import net.minecraft.server.v1_12_R1.EntitySkeleton;
import net.minecraft.server.v1_12_R1.EntitySkeletonStray;
import net.minecraft.server.v1_12_R1.EntitySkeletonWither;
import net.minecraft.server.v1_12_R1.EntitySlime;
import net.minecraft.server.v1_12_R1.EntitySmallFireball;
import net.minecraft.server.v1_12_R1.EntitySnowball;
import net.minecraft.server.v1_12_R1.EntitySnowman;
import net.minecraft.server.v1_12_R1.EntitySpectralArrow;
import net.minecraft.server.v1_12_R1.EntitySpider;
import net.minecraft.server.v1_12_R1.EntitySquid;
import net.minecraft.server.v1_12_R1.EntityTNTPrimed;
import net.minecraft.server.v1_12_R1.EntityThrownExpBottle;
import net.minecraft.server.v1_12_R1.EntityTypes;
import net.minecraft.server.v1_12_R1.EntityVex;
import net.minecraft.server.v1_12_R1.EntityVillager;
import net.minecraft.server.v1_12_R1.EntityVindicator;
import net.minecraft.server.v1_12_R1.EntityWitch;
import net.minecraft.server.v1_12_R1.EntityWither;
import net.minecraft.server.v1_12_R1.EntityWitherSkull;
import net.minecraft.server.v1_12_R1.EntityWolf;
import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.EntityZombieHusk;
import net.minecraft.server.v1_12_R1.EntityZombieVillager;
import net.minecraft.server.v1_12_R1.MinecraftKey;

/**
 * A Free-to-use library class for registering custom entities in Minecraft, using the Spigot server software (ver.
 * 1.12-pre) as its API.
 *
 * @author jetp250
 */
// TODO Homework: Implement this for the 1.12 version of killer snowman
public class NMSUtil1_12 {

	private static final Field META_LIST_MONSTER;
	private static final Field META_LIST_CREATURE;
	private static final Field META_LIST_WATER_CREATURE;
	private static final Field META_LIST_AMBIENT;

	private static final BiomeBase[] BIOMES;

	/**
	 * Registers the custom class to be available for use.
	 *
	 * @param name
	 *            - The 'savegame id' of the mob.
	 * @param type
	 *            - The type of your mob
	 * @param customClass
	 *            - Your custom class that'll be used
	 * @param override
	 *            - Should your mob be set as a default in each biome? Only one custom entity of this type entity can
	 *            have this set as 'true'.
	 *
	 * @see EntityType#getName() EntityType#getName() for the savegame id.
	 */

	public static void registerEntity(String name, Type type, Class<? extends Entity> customClass, boolean override) {
		final MinecraftKey key = new MinecraftKey(name);
		EntityTypes.b.a(type.getId(), key, customClass);
		if (!EntityTypes.d.contains(key))
			EntityTypes.d.add(key);
		if (!override || type.isSpecial())
			return;
		Field field;
		if ((field = type.getMeta().getField()) == null)
			return;
		try {
			field.setAccessible(true);
			for (final BiomeBase base : NMSUtil1_12.BIOMES) {
				final List<BiomeMeta> list = (List<BiomeMeta>) field.get(base);
				for (final BiomeMeta meta : list)
					if (meta.b == type.getNMSClass()) {
						meta.b = (Class<? extends EntityInsentient>) customClass;
						break;
					}
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public enum Type {
		DROPPED_ITEM(1, "item", EntityItem.class, MobMeta.UNDEFINED, true),
		EXPERIENCE_ORB(2, "xp_orb", EntityExperienceOrb.class, MobMeta.UNDEFINED, true),
		AREA_EFFECT_CLOUD(3, "area_effect_cloud", EntityAreaEffectCloud.class, MobMeta.UNDEFINED, true),
		ELDER_GUARDIAN(4, "elder_guardian", EntityGuardianElder.class, MobMeta.MONSTER, false),
		WITHER_SKELETON(5, "wither_skeleton", EntitySkeletonWither.class, MobMeta.MONSTER, false),
		STRAY(6, "stray", EntitySkeletonStray.class, MobMeta.MONSTER, false),
		THROWN_EGG(7, "egg", EntityEgg.class, MobMeta.UNDEFINED, true),
		LEAD_KNOT(8, "leash_knot", EntityLeash.class, MobMeta.UNDEFINED, true),
		PAINTING(9, "painting", EntityPainting.class, MobMeta.UNDEFINED, true),
		ARROW(10, "arrow", EntityArrow.class, MobMeta.UNDEFINED, true),
		SNOWBALL(11, "snowball", EntitySnowball.class, MobMeta.UNDEFINED, true),
		FIREBALL(12, "fireball", EntityFireball.class, MobMeta.UNDEFINED, true),
		SMALL_FIREBALL(13, "fireball", EntitySmallFireball.class, MobMeta.UNDEFINED, true),
		ENDER_PEARL(14, "ender_pearl", EntityEnderPearl.class, MobMeta.UNDEFINED, true),
		EYE_OF_ENDER(15, "eye_of_ender_signal", EntityEnderSignal.class, MobMeta.UNDEFINED, true),
		POTION(16, "potion", EntityPotion.class, MobMeta.UNDEFINED, true),
		EXP_BOTTLE(17, "xp_bottle", EntityThrownExpBottle.class, MobMeta.UNDEFINED, true),
		ITEM_FRAME(18, "item_frame", EntityItemFrame.class, MobMeta.UNDEFINED, true),
		WITHER_SKULL(19, "wither_skull", EntityWitherSkull.class, MobMeta.UNDEFINED, true),
		PRIMED_TNT(20, "tnt", EntityTNTPrimed.class, MobMeta.UNDEFINED, true),
		FALLING_BLOCK(21, "falling_block", EntityFallingBlock.class, MobMeta.UNDEFINED, true),
		FIREWORK_ROCKET(22, "fireworks_rocket", EntityFireworks.class, MobMeta.UNDEFINED, true),
		HUSK(23, "husk", EntityZombieHusk.class, MobMeta.MONSTER, false),
		SPECTRAL_ARROW(24, "spectral_arrow", EntitySpectralArrow.class, MobMeta.UNDEFINED, true),
		SHULKER_BULLET(25, "shulker_bullet", EntityShulkerBullet.class, MobMeta.UNDEFINED, true),
		DRAGON_FIREBALL(26, "dragon_fireball", EntityDragonFireball.class, MobMeta.UNDEFINED, true),
		ZOMBIE_VILLAGER(27, "zombie_villager", EntityZombieVillager.class, MobMeta.MONSTER, false),
		SKELETON_HORSE(28, "skeleton_horse", EntityHorseSkeleton.class, MobMeta.CREATURE, false),
		ZOMBIE_HORSE(29, "zombie_horse", EntityHorseZombie.class, MobMeta.CREATURE, false),
		ARMOR_STAND(30, "armor_stand", EntityArmorStand.class, MobMeta.UNDEFINED, true),
		DONKEY(31, "donkey", EntityHorseDonkey.class, MobMeta.CREATURE, false),
		MULE(32, "mule", EntityHorseMule.class, MobMeta.CREATURE, false),
		EVOCATION_FANGS(33, "evocation_fangs", EntityEvokerFangs.class, MobMeta.UNDEFINED, true),
		EVOKER(34, "evocation_illager", EntityEvoker.class, MobMeta.MONSTER, false),
		VEX(35, "vex", EntityVex.class, MobMeta.MONSTER, false),
		VINDICATOR(36, "vindication_illager", EntityVindicator.class, MobMeta.MONSTER, false),
		ILLUSIONER(37, "illusion_illager", EntityIllagerIllusioner.class, MobMeta.MONSTER, false),
		COMMAND_BLOCK_MINECART(40, "commandblock_minecart", EntityMinecartCommandBlock.class, MobMeta.UNDEFINED, true),
		BOAT(41, "boat", EntityBoat.class, MobMeta.UNDEFINED, true),
		MINECART(42, "minecart", EntityMinecartRideable.class, MobMeta.UNDEFINED, true),
		CHEST_MINECART(43, "chest_minecart", EntityMinecartChest.class, MobMeta.UNDEFINED, true),
		FURNACE_MINECART(44, "furnace_minecart", EntityMinecartFurnace.class, MobMeta.UNDEFINED, true),
		TNT_MINECART(45, "tnt_minecart", EntityMinecartTNT.class, MobMeta.UNDEFINED, true),
		HOPPER_MINECART(46, "hopper_minecart", EntityMinecartHopper.class, MobMeta.UNDEFINED, true),
		SPAWNER_MINECART(47, "spawner_minecart", EntityMinecartMobSpawner.class, MobMeta.UNDEFINED, true),
		CREEPER(50, "creeper", EntityCreeper.class, MobMeta.MONSTER, false),
		SKELETON(51, "skeleton", EntitySkeleton.class, MobMeta.MONSTER, false),
		SPIDER(52, "spider", EntitySpider.class, MobMeta.MONSTER, false),
		GIANT(53, "giant", EntityGiantZombie.class, MobMeta.MONSTER, false),
		ZOMBIE(54, "zombie", EntityZombie.class, MobMeta.MONSTER, false),
		SLIME(55, "slime", EntitySlime.class, MobMeta.MONSTER, false),
		GHAST(56, "ghast", EntityGhast.class, MobMeta.MONSTER, false),
		PIG_ZOMBIE(57, "zombie_pigman", EntityPigZombie.class, MobMeta.MONSTER, false),
		ENDERMAN(58, "enderman", EntityEnderman.class, MobMeta.MONSTER, false),
		CAVE_SPIDER(59, "cave_spider", EntityCaveSpider.class, MobMeta.MONSTER, false),
		SILVERFISH(60, "silverfish", EntitySilverfish.class, MobMeta.MONSTER, false),
		BLAZE(61, "blaze", EntityBlaze.class, MobMeta.MONSTER, false),
		MAGMACUBE(62, "magma_cube", EntityMagmaCube.class, MobMeta.MONSTER, false),
		ENDER_DRAGON(63, "ender_dragon", EntityEnderDragon.class, MobMeta.MONSTER, false),
		WITHER(64, "wither", EntityWither.class, MobMeta.MONSTER, false),
		BAT(65, "bat", EntityBat.class, MobMeta.AMBIENT, false),
		WITCH(66, "witch", EntityWitch.class, MobMeta.MONSTER, false),
		ENDERMITE(67, "endermite", EntityEndermite.class, MobMeta.MONSTER, false),
		GUARDIAN(68, "guardian", EntityGuardian.class, MobMeta.MONSTER, false),
		SHULKER(69, "shulker", EntityShulker.class, MobMeta.MONSTER, false),
		PIG(90, "pig", EntityPig.class, MobMeta.CREATURE, false),
		SHEEP(91, "sheep", EntitySheep.class, MobMeta.CREATURE, false),
		COW(92, "cow", EntityCow.class, MobMeta.CREATURE, false),
		CHICKEN(93, "chicken", EntityChicken.class, MobMeta.CREATURE, false),
		SQUID(94, "squid", EntitySquid.class, MobMeta.WATER_CREATURE, false),
		WOLF(95, "wolf", EntityWolf.class, MobMeta.CREATURE, false),
		MOOSHROOM(96, "mooshroom", EntityMushroomCow.class, MobMeta.CREATURE, false),
		SNOWMAN(97, "snowman", EntitySnowman.class, MobMeta.CREATURE, false),
		OCELOT(98, "ocelot", EntityOcelot.class, MobMeta.CREATURE, false),
		IRON_GOLEM(99, "villager_golem", EntityIronGolem.class, MobMeta.CREATURE, false),
		HORSE(100, "horse", EntityHorse.class, MobMeta.CREATURE, false),
		RABBIT(101, "rabbit", EntityRabbit.class, MobMeta.CREATURE, false),
		POLARBEAR(102, "polar_bear", EntityPolarBear.class, MobMeta.CREATURE, false),
		LLAMA(103, "llama", EntityLlama.class, MobMeta.CREATURE, false),
		LLAMA_SPIT(104, "llama_spit", EntityLlamaSpit.class, MobMeta.UNDEFINED, true),
		PARROT(105, "parrot", EntityParrot.class, MobMeta.CREATURE, false),
		VILLAGER(120, "villager", EntityVillager.class, MobMeta.CREATURE, false),
		ENDER_CRYSTAL(200, "ender_crystal", EntityEnderCrystal.class, MobMeta.UNDEFINED, true);

		private final int id;
		private final String name;
		private final Class<? extends Entity> clazz;
		private final MobMeta meta;
		private final boolean special;

		Type(int id, String name, Class<? extends Entity> nmsClazz, MobMeta meta, boolean special) {
			this.id = id;
			this.name = name;
			this.clazz = nmsClazz;
			this.meta = meta;
			this.special = special;
		}

		public MobMeta getMeta() {
			return this.meta;
		}

		public int getId() {
			return this.id;
		}

		public String getName() {
			return this.name;
		}

		public Class<? extends Entity> getNMSClass() {
			return this.clazz;
		}

		public boolean isSpecial() {
			return this.special;
		}
	}

	public enum MobMeta {
		MONSTER(NMSUtil1_12.META_LIST_MONSTER),
		CREATURE(NMSUtil1_12.META_LIST_CREATURE),
		WATER_CREATURE(NMSUtil1_12.META_LIST_WATER_CREATURE),
		AMBIENT(NMSUtil1_12.META_LIST_AMBIENT),
		UNDEFINED(null);

		private final Field field;

		MobMeta(Field field) {
			this.field = field;
		}

		/**
		 * @return the BiomeMeta list field of this entity.
		 *         <p>
		 *         <b>Undefined will not be accepted and returns null.</b>
		 *         </p>
		 */
		public Field getField() {
			return this.field;
		}
	}

	public class NBTTagType {
		public static final int COMPOUND = 10;
		public static final int LIST = 9;
		public static final int STRING = 8;
		public static final int LONG_ARRAY = 12;
		public static final int INT_ARRAY = 11;
		public static final int BYTE_ARRAY = 7;
		public static final int DOUBLE = 6;
		public static final int FLOAT = 5;
		public static final int LONG = 4;
		public static final int INT = 3;
		public static final int SHORT = 2;
		public static final int BYTE = 1;
		public static final int BOOLEAN = 1;
		public static final int END = 0;
	}

	static {
		final Class<BiomeBase> clazz = BiomeBase.class;
		Field monster = null;
		Field creature = null;
		Field water = null;
		Field ambient = null;

		try {
			// These fields may vary depending on your version.
			// The new names can be found under
			// net.minecraft.server.<version>.BiomeBase.class
			monster = clazz.getDeclaredField("t");
			creature = clazz.getDeclaredField("u");
			water = clazz.getDeclaredField("v");
			ambient = clazz.getDeclaredField("w");
		} catch (final Exception e) {
			Bukkit.getLogger().warning("Wrong server version / software; BiomeMeta fields not found, aborting.");
		}

		META_LIST_MONSTER = monster;
		META_LIST_CREATURE = creature;
		META_LIST_WATER_CREATURE = water;
		META_LIST_AMBIENT = ambient;
		BIOMES = new BiomeBase[BiomeBase.i.a()];

		final Iterator<BiomeBase> iterator = BiomeBase.i.iterator();
		int index = 0;
		while (iterator.hasNext())
			NMSUtil1_12.BIOMES[index++] = iterator.next();
	}
}