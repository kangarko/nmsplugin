package org.mineacademy.nms.specific.entity;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntitySnowball;
import net.minecraft.server.v1_8_R3.EntitySnowman;
import net.minecraft.server.v1_8_R3.MathHelper;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;

/**
 * Represents a customized snowman entity.
 */
final class KillerSnowman_v1_8 extends EntitySnowman implements NMSEntity {

	/**
	 * Create a new snowman at the given location.
	 * <p>
	 * **You must call this using {@link CustomEntity} class for it to be
	 * properly registered!**
	 *
	 * @param location
	 */
	public KillerSnowman_v1_8(final Location location) {
		super(((org.bukkit.craftbukkit.v1_8_R3.CraftWorld) location.getWorld()).getHandle());

		// How entity moves
		this.goalSelector.a(1, new PathfinderGoalMeleeAttack(this, 10, true));

		// How entity targets other entities
		this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget<>(this, EntityHuman.class, true));

		// Unsupported on 1.8
		// this.setHasPumpkin(false);

		// Despawn flag
		this.persistent = false;

		// You can also get the Bukkit entity at this point and manipulate it
		final Entity bukkitEntity = getBukkitEntity();

		bukkitEntity.setCustomName("Killer Snowman");
		bukkitEntity.setCustomNameVisible(true);

		// Inject invisible flag that Bukkit recognizes and saves to make this mob
		// persistent over restarts
		CompMetadata.setMetadata(bukkitEntity, "CustomEntity", CustomEntity.SNOWMAN.toString());

	}

	/**
	 * Handles this snowmen shooting snowballs
	 *
	 * @see net.minecraft.server.v1_16_R3.EntitySnowman#a(net.minecraft.server.v1_16_R3.EntityLiving, float)
	 */
	@Override
	public void a(final EntityLiving entityliving, final float f) {
		final EntitySnowball snowball = new EntitySnowball(this.world, this);
		final double d0 = entityliving.locY + entityliving.getHeadHeight() - 1.100000023841858D;
		final double d1 = entityliving.locX - this.locX;
		final double d2 = d0 - snowball.locY;
		final double d3 = entityliving.locZ - this.locZ;
		final float f1 = MathHelper.sqrt(d1 * d1 + d3 * d3) * 0.2F;

		snowball.shoot(d1, d2 + f1, d3, 1.6F, 12.0F);

		this.makeSound("mob.wither.shoot", 1.0F, 1.0F / (this.bc().nextFloat() * 0.4F + 0.8F));
		this.world.addEntity(snowball);

		// We inject our snowball tracking to damage entities which are hit
		EntityUtil.trackHit((Projectile) snowball.getBukkitEntity(), hitEvent -> {
			final Entity hitEntity = Remain.getHitEntity(hitEvent);

			if (hitEntity instanceof LivingEntity) {
				final LivingEntity living = (LivingEntity) hitEntity;

				living.setHealth(MathUtil.range(living.getHealth() - 4, 0, living.getHealth()));
			}
		});
	}

	@Override
	public LivingEntity spawn(final Location location) {
		this.setPosition(location.getX(), location.getY(), location.getZ());
		this.getWorld().addEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);

		return (LivingEntity) this.getBukkitEntity();
	}

	@Override
	protected String z() {
		return "mob.wither.idle";
	}

	@Override
	protected String bo() {
		return "mob.wither.hurt";
	}

	@Override
	protected String bp() {
		return "mob.wither.death";
	}
}
