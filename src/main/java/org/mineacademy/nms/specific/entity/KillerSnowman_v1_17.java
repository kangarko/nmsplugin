/*package org.mineacademy.nms.specific.entity;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.remain.CompMetadata;

import net.minecraft.server.v1_16_R3.EnumMobSpawn;
import net.minecraft.server.v1_16_R3.EnumMoveType;
import net.minecraft.server.v1_16_R3.SoundEffects;
import net.minecraft.server.v1_16_R3.TagsFluid;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.level.GameRules;


final class KillerSnowman_v1_17 extends EntitySnowman implements NMSEntity {


	public KillerSnowman_v1_17(final Location location) {
		super(EntityTypes.aF, ((CraftWorld) location.getWorld()).getHandle());

		// How entity moves
		this.bP.a(1, new PathfinderGoalMeleeAttack(this, 10, true));

		// How entity targets other entities
		this.bQ.a(2, new PathfinderGoalNearestAttackableTarget<>(this, EntityHuman.class, true));

		// Remove the pumpkin
		this.setHasPumpkin(false);

		// Despawn flag
		this.persist = false;

		// You can also get the Bukkit entity at this point and manipulate it
		final Entity bukkitEntity = getBukkitEntity();

		bukkitEntity.setCustomName("Killer Snowman");
		bukkitEntity.setCustomNameVisible(true);

		// Inject invisible flag that Bukkit recognizes and saves to make this mob
		// persistent over restarts
		CompMetadata.setMetadata(bukkitEntity, "CustomEntity", CustomEntity.SNOWMAN.toString());

		this.getAttributeMap().registerAttribute(GenericAttributes.l);
	}

	EntityZombie a;


	@Override
	public boolean damageEntity(final DamageSource source, final float amount) {
		if (!super.damageEntity(source, amount))
			return false;
		else if (!(super.t instanceof WorldServer))
			return false;
		else {
			final WorldServer worldserver = (WorldServer) super.t;
			EntityLiving entityliving = this.getGoalTarget();
			if (entityliving == null && source.getEntity() instanceof EntityLiving)
				entityliving = (EntityLiving) source.getEntity();

			if (entityliving != null && super.t.getDifficulty() == EnumDifficulty.d && super.Q.nextFloat() < this.b(GenericAttributes.l) && super.t.getGameRules().getBoolean(GameRules.e)) {
				final int i = MathHelper.floor(this.locX());
				final int j = MathHelper.floor(this.locY());
				final int k = MathHelper.floor(this.locZ());
				final EntityChicken entityzombie = new EntityChicken(EntityTypes.l, super.t);

				for (int l = 0; l < 50; ++l) {
					final int i1 = i + MathHelper.nextInt(super.Q, 7, 40) * MathHelper.nextInt(super.Q, -1, 1);
					final int j1 = j + MathHelper.nextInt(super.Q, 7, 40) * MathHelper.nextInt(super.Q, -1, 1);
					final int k1 = k + MathHelper.nextInt(super.Q, 7, 40) * MathHelper.nextInt(super.Q, -1, 1);
					final BlockPosition blockposition = new BlockPosition(i1, j1, k1);
					final EntityTypes<?> entitytypes = entityzombie.getEntityType();
					final EntityPositionTypes.Surface entitypositiontypes_surface = EntityPositionTypes.a(entitytypes);

					if (SpawnerCreature.a(entitypositiontypes_surface, super.t, blockposition, entitytypes) && EntityPositionTypes.a(entitytypes, worldserver, EnumMobSpawn.j, blockposition, super.t.w)) {
						entityzombie.setPosition(i1, j1, k1);
						if (!super.t.isPlayerNearby(i1, j1, k1, 7.0D) && super.t.f(entityzombie) && super.t.getCubes(entityzombie) && !super.t.containsLiquid(entityzombie.getBoundingBox())) {
							entityzombie.setGoalTarget(entityliving, EntityTargetEvent.TargetReason.REINFORCEMENT_TARGET, true);
							entityzombie.prepare(worldserver, super.t.getDamageScaler(entityzombie.getChunkCoordinates()), EnumMobSpawn.j, (GroupDataEntity) null, (NBTTagCompound) null);
							worldserver.addAllEntities(entityzombie, CreatureSpawnEvent.SpawnReason.REINFORCEMENTS);
							this.getAttributeInstance(GenericAttributes.l).addModifier(new AttributeModifier("Zombie reinforcement caller charge", -0.05000000074505806D, AttributeModifier.Operation.a));
							entityzombie.getAttributeInstance(GenericAttributes.l).addModifier(new AttributeModifier("Zombie reinforcement callee charge", -0.05000000074505806D, AttributeModifier.Operation.a));
							break;
						}
					}
				}
			}

			return true;
		}
	}


	@Override
	public void a(final EntityLiving entityliving, final float f) {
		final EntitySnowball snowball = new EntitySnowball(this.t, this);

		final double posX = entityliving.locX() - this.locX();
		final double posY = (entityliving.locY() + entityliving.getHeadHeight() - 1.100000023841858) - snowball.locY();
		final double posZ = entityliving.locZ() - this.locZ();
		final float f2 = (float) Math.sqrt(posX * posX + posZ * posZ) * 0.2f;

		snowball.shoot(posX, posY + f2, posZ, 1.6f, 12.0f);

		// Customize the code here to customize shooting sound
		this.playSound(SoundEffects.qE, 1.0f, 1.0f / (this.getRandom().nextFloat() * 0.4f + 0.8f));
		this.t.addEntity(snowball);

		// We inject our snowball tracking to damage entities which are hit
		EntityUtil.trackHit((Projectile) snowball.getBukkitEntity(), hitEvent -> {
			final Entity hitEntity = hitEvent.getHitEntity();

			if (hitEntity instanceof LivingEntity) {
				final LivingEntity living = (LivingEntity) hitEntity;

				living.setHealth(MathUtil.range(living.getHealth() - 4, 0, living.getHealth()));
			}
		});

	}

	@Override
	public void g(final Vec3D vec3d) {

		final double motionSpeed = 0.2;
		final double speed = 1.0;
		final double jumpVelocity = 0.7;

		// Do not call for dead entities
		if (!this.isAlive())
			return;

		// If there's not passenger, we'll default to NMS behavior
		if (!this.isVehicle()) {
			this.bb = 0.02F;
			super.g(vec3d);

			return;
		}

		final EntityLiving passenger = (EntityLiving) this.getPassengers().get(0);

		// Reset fall distance when back on ground
		if (this.isOnGround())
			this.K = 0;

		// Float up, when in water
		if (this.a(TagsFluid.b))
			this.setMot(this.getMot().add(0, 0.5, 0));

		// Make this mob look where the rider is looking
		this.setYawPitch(passenger.getBukkitYaw(), passenger.getBukkitEntity().getLocation().getPitch() * 0.5F);

		this.aP = this.getBukkitYaw();
		this.bt = this.getBukkitYaw();

		final double sideMotion = passenger.bo * motionSpeed;
		double forwardMotion = passenger.bq * motionSpeed;

		// If we are going backwards, make backwards walk slower
		if (forwardMotion <= 0F)
			forwardMotion *= 0.25F;

		this.ride(sideMotion, forwardMotion, vec3d.getY(), (float) speed);

		// Handle jumping
		if (passenger.bn && this.isOnGround()) // prevent infinite jump
			this.setMot(this.getMot().getX(), jumpVelocity, this.getMot().getZ());

		this.a(this, false);
	}

	// Inspired from pig, horse classes and
	// https://www.spigotmc.org/threads/1-15-entity-wasd-control.431302/
	// Updated and reworked for Minecraft 1.16 by MineAcademy
	private void ride(final double motionSideways, final double motionForward, final double motionUpwards, final float speedModifier) {

		double locY;
		float f2;
		final float speed;
		final float swimSpeed;

		// Behavior in water
		if (this.a(TagsFluid.b)) {
			locY = this.locY();
			speed = 0.8F;
			swimSpeed = 0.02F;

			this.a(swimSpeed, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.a, this.getMot());

			final double motX = this.getMot().getX() * speed;
			double motY = this.getMot().getY() * 0.800000011920929D;
			final double motZ = this.getMot().getZ() * speed;
			motY -= 0.02D;

			if (this.A && this.f(this.getMot().getX(), this.getMot().getY() + 0.6000000238418579D - this.locY() + locY, this.getMot().getZ()))
				motY = 0.30000001192092896D;

			this.setMot(motX, motY, motZ);

			// Behavior in lava
		} else if (this.a(TagsFluid.c)) {
			locY = this.locY();

			this.a(2F, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.a, this.getMot());

			final double motX = this.getMot().getX() * 0.5D;
			double motY = this.getMot().getY() * 0.5D;
			final double motZ = this.getMot().getZ() * 0.5D;

			motY -= 0.02D;

			if (this.A && this.f(this.getMot().getX(), this.getMot().getY() + 0.6000000238418579D - this.locY() + locY, this.getMot().getZ()))
				motY = 0.30000001192092896D;

			this.setMot(motX, motY, motZ);

			// Behavior on land
		} else {
			final float friction = 0.91F;

			speed = speedModifier * (0.16277136F / (friction * friction * friction));

			this.a(speed, new Vec3D(motionSideways, motionUpwards, motionForward));

			double motX = this.getMot().getX();
			double motY = this.getMot().getY();
			double motZ = this.getMot().getZ();

			if (this.isClimbing()) {
				swimSpeed = 0.15F;
				motX = MathHelper.a(motX, -swimSpeed, swimSpeed);
				motZ = MathHelper.a(motZ, -swimSpeed, swimSpeed);
				this.K = 0.0F;

				if (motY < -0.15D)
					motY = -0.15D;
			}

			final Vec3D mot = new Vec3D(motX, motY, motZ);

			this.move(EnumMoveType.a, mot);

			if (this.A && this.isClimbing())
				motY = 0.2D;

			motY -= 0.08D;

			motY *= 0.9800000190734863D;
			motX *= friction;
			motZ *= friction;

			this.setMot(motX, motY, motZ);
		}

		this.aP = this.ba;
		locY = this.locX() - this.u;
		final double d1 = this.locZ() - this.w;
		f2 = (float) Math.sqrt(locY * locY + d1 * d1) * 4.0F;

		if (f2 > 1.0F)
			f2 = 1.0F;

		this.ba += (f2 - this.ba) * 0.4F;
		this.bb += this.ba;
	}

	@Override
	public LivingEntity spawn(final Location location) {
		this.setPosition(location.getX(), location.getY(), location.getZ());
		this.getWorld().addEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);

		return this.getBukkitLivingEntity();
	}


	@Override
	protected SoundEffect getSoundFall(final int fallHeight) {
		return SoundEffects.uO;
	}


	@Override
	protected SoundEffect getSoundAmbient() {
		return SoundEffects.uA; // laughter
	}


	@Override
	public SoundEffect getSoundDeath() {
		return SoundEffects.uM;
	}
}*/
