package org.mineacademy.nms.specific.entity;

import java.lang.reflect.Constructor;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.exception.FoException;

import net.minecraft.server.v1_8_R3.EntitySnowman;

/**
 * A custom entity registry to spawn customized NMS entities.
 */
public enum CustomEntity {

	/**
	 * Represents a customized Killer Snowman entity.
	 */
	SNOWMAN {
		@Override
		protected Class<? extends NMSEntity> getEntityClass() {

			if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
				return KillerSnowman_v1_8.class;

			else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
				return KillerSnowman_v1_16.class;

			else if ((MinecraftVersion.equals(MinecraftVersion.V.v1_20) && MinecraftVersion.getSubversion() >= 5) || MinecraftVersion.newerThan(MinecraftVersion.V.v1_20))
				return KillerSnowman_Modern.class;

			throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());
		}
	};

	static {
		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			NMSUtil1_8.registerEntity("psycho", 97, EntitySnowman.class, KillerSnowman_v1_8.class);
	}

	protected abstract Class<? extends NMSEntity> getEntityClass();

	/**
	 * Adds the given entity to the world and renders it visible for players.
	 * Minecraft handles the rest (such as saving/loading) of this entity.
	 * <p>
	 * YOU MUST HAVE A PUBLIC CONSTRUCTOR ONLY TAKING IN A LOCATION CLASS
	 * FOR YOUR ENTITY FOR THIS METHOD TO WORK.
	 *
	 * @param location
	 * @return
	 */
	public LivingEntity spawn(final Location location) {
		final Constructor<?> constructor = ReflectionUtil.getConstructor(this.getEntityClass(), Location.class);
		final NMSEntity entity = (NMSEntity) ReflectionUtil.instantiate(constructor, location);

		return entity.spawn(location);
	}
}
