package org.mineacademy.nms.specific.entity;

import org.bukkit.Location;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.remain.CompMetadata;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.SnowGolem;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Snowball;

/**
 * Represents a customized snowman entity.
 */
final class KillerSnowman_Modern extends SnowGolem implements NMSEntity {

	/**
	 * Create a new snowman at the given location.
	 * <p>
	 * **You must call this using {@link CustomEntity} class for it to be
	 * properly registered!**
	 *
	 * @param location
	 */
	public KillerSnowman_Modern(final Location location) {
		super(EntityType.SNOW_GOLEM, ((CraftWorld) location.getWorld()).getHandle());

		// How entity moves
		this.goalSelector.addGoal(1, new MeleeAttackGoal(this, 10, true));

		// How entity targets other entities
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Player.class, true));

		// Remove the pumpkin
		this.setPumpkin(false);

		// Despawn flag
		this.persist = false;

		// You can also get the Bukkit entity at this point and manipulate it
		final Entity bukkitEntity = getBukkitEntity();

		bukkitEntity.setCustomName("Killer Snowman");
		bukkitEntity.setCustomNameVisible(true);

		// Inject invisible flag that Bukkit recognizes and saves to make this mob
		// persistent over restarts
		CompMetadata.setMetadata(bukkitEntity, "CustomEntity", CustomEntity.SNOWMAN.toString());

		// Unneeded
		//this.getAttributes().registerAttribute(Attributes.SPAWN_REINFORCEMENTS_CHANCE);
	}

	/**
	 * Handles this snowmen shooting snowballs
	 */
	@Override
	public void performRangedAttack(net.minecraft.world.entity.LivingEntity entityliving, float f) {
		final Snowball snowball = new Snowball(this.level(), this);

		final double posX = entityliving.getEyeY() - 1.100000023841858D;
		final double posY = entityliving.getX() - getX();
		final double posZ = posX - snowball.getY();
		final double f2 = entityliving.getZ() - getZ();
		final double f3 = Math.sqrt(posY * posY + f2 * f2) * 0.20000000298023224D;

		snowball.shoot(posY, posZ + f3, f2, 1.6F, 12.0F);

		// Customize the code here to customize shooting sound
		playSound(SoundEvents.WITHER_SHOOT, 1.0F, 0.4F / (getRandom().nextFloat() * 0.4F + 0.8F));
		this.level().addFreshEntity(snowball);

		// We inject our snowball tracking to damage entities which are hit
		EntityUtil.trackHit((Projectile) snowball.getBukkitEntity(), hitEvent -> {
			final Entity hitEntity = hitEvent.getHitEntity();

			if (hitEntity instanceof LivingEntity) {
				final LivingEntity living = (LivingEntity) hitEntity;

				living.damage(4, this.getBukkitEntity());
			}
		});
	}

	/*@Override
	public void g(final Vec3D vec3d) {
	
		final double motionSpeed = 0.2;
		final double speed = 1.0;
		final double jumpVelocity = 0.7;
	
		// Do not call for dead entities
		if (!this.isAlive())
			return;
	
		// If there's not passenger, we'll default to NMS behavior
		if (!this.isVehicle()) {
			this.bb = 0.02F;
			super.g(vec3d);
	
			return;
		}
	
		final EntityLiving passenger = (EntityLiving) this.getPassengers().get(0);
	
		// Reset fall distance when back on ground
		if (this.isOnGround())
			this.K = 0;
	
		// Float up, when in water
		if (this.a(TagsFluid.b))
			this.setMot(this.getMot().add(0, 0.5, 0));
	
		// Make this mob look where the rider is looking
		this.setYawPitch(passenger.getBukkitYaw(), passenger.getBukkitEntity().getLocation().getPitch() * 0.5F);
	
		this.aP = this.getBukkitYaw();
		this.bt = this.getBukkitYaw();
	
		final double sideMotion = passenger.bo * motionSpeed;
		double forwardMotion = passenger.bq * motionSpeed;
	
		// If we are going backwards, make backwards walk slower
		if (forwardMotion <= 0F)
			forwardMotion *= 0.25F;
	
		this.ride(sideMotion, forwardMotion, vec3d.getY(), (float) speed);
	
		// Handle jumping
		if (passenger.bn && this.isOnGround()) // prevent infinite jump
			this.setMot(this.getMot().getX(), jumpVelocity, this.getMot().getZ());
	
		this.a(this, false);
	}*/

	// Inspired from pig, horse classes and
	// https://www.spigotmc.org/threads/1-15-entity-wasd-control.431302/
	// Updated and reworked for Minecraft 1.16 by MineAcademy
	/*private void ride(final double motionSideways, final double motionForward, final double motionUpwards, final float speedModifier) {
	
		double locY;
		float f2;
		final float speed;
		final float swimSpeed;
	
		// Behavior in water
		if (this.a(TagsFluid.b)) {
			locY = this.locY();
			speed = 0.8F;
			swimSpeed = 0.02F;
	
			this.a(swimSpeed, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.a, this.getMot());
	
			final double motX = this.getMot().getX() * speed;
			double motY = this.getMot().getY() * 0.800000011920929D;
			final double motZ = this.getMot().getZ() * speed;
			motY -= 0.02D;
	
			if (this.A && this.f(this.getMot().getX(), this.getMot().getY() + 0.6000000238418579D - this.locY() + locY, this.getMot().getZ()))
				motY = 0.30000001192092896D;
	
			this.setMot(motX, motY, motZ);
	
			// Behavior in lava
		} else if (this.a(TagsFluid.c)) {
			locY = this.locY();
	
			this.a(2F, new Vec3D(motionSideways, motionUpwards, motionForward));
			this.move(EnumMoveType.a, this.getMot());
	
			final double motX = this.getMot().getX() * 0.5D;
			double motY = this.getMot().getY() * 0.5D;
			final double motZ = this.getMot().getZ() * 0.5D;
	
			motY -= 0.02D;
	
			if (this.A && this.f(this.getMot().getX(), this.getMot().getY() + 0.6000000238418579D - this.locY() + locY, this.getMot().getZ()))
				motY = 0.30000001192092896D;
	
			this.setMot(motX, motY, motZ);
	
			// Behavior on land
		} else {
			final float friction = 0.91F;
	
			speed = speedModifier * (0.16277136F / (friction * friction * friction));
	
			this.a(speed, new Vec3D(motionSideways, motionUpwards, motionForward));
	
			double motX = this.getMot().getX();
			double motY = this.getMot().getY();
			double motZ = this.getMot().getZ();
	
			if (this.isClimbing()) {
				swimSpeed = 0.15F;
				motX = MathHelper.a(motX, -swimSpeed, swimSpeed);
				motZ = MathHelper.a(motZ, -swimSpeed, swimSpeed);
				this.K = 0.0F;
	
				if (motY < -0.15D)
					motY = -0.15D;
			}
	
			final Vec3D mot = new Vec3D(motX, motY, motZ);
	
			this.move(EnumMoveType.a, mot);
	
			if (this.A && this.isClimbing())
				motY = 0.2D;
	
			motY -= 0.08D;
	
			motY *= 0.9800000190734863D;
			motX *= friction;
			motZ *= friction;
	
			this.setMot(motX, motY, motZ);
		}
	
		this.aP = this.ba;
		locY = this.locX() - this.u;
		final double d1 = this.locZ() - this.w;
		f2 = (float) Math.sqrt(locY * locY + d1 * d1) * 4.0F;
	
		if (f2 > 1.0F)
			f2 = 1.0F;
	
		this.ba += (f2 - this.ba) * 0.4F;
		this.bb += this.ba;
	}*/

	@Override
	public LivingEntity spawn(final Location location) {
		this.setPos(location.getX(), location.getY(), location.getZ());
		this.level().addFreshEntity(this, CreatureSpawnEvent.SpawnReason.CUSTOM);

		return (LivingEntity) this.getBukkitEntity();
	}

	/**
	 * Override this method to customize falling/step sound
	 */
	@Override
	public Fallsounds getFallSounds() {
		return new Fallsounds(SoundEvents.CHAIN_FALL, SoundEvents.CHAIN_BREAK);
	}

	/**
	 * Override this method to customize ambient/silent sound
	 */
	@Override
	protected SoundEvent getAmbientSound() {
		return SoundEvents.WITHER_AMBIENT; // laughter
	}

	/**
	 * Override this method to customize death sound
	 */
	@Override
	public SoundEvent getDeathSound() {
		return SoundEvents.WITHER_DEATH;
	}
}
