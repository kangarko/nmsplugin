package org.mineacademy.nms.specific.goal;

import java.util.UUID;

import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.craftbukkit.entity.CraftWolf;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.animal.Wolf;

/**
 * Represents a custom pathfinder goal to follow an owner for pets.
 */
final class PathfinderGoalPet_Modern extends SimplePathfinderGoal_Modern {

	/**
	 * How fast should the pet walk to the owner
	 */
	private final double speed;

	/**
	 * The maximum distance the pet should walk from.
	 * If the pet is farther than this, we'll teleport it.
	 */
	private final double navigationDistanceThreshold;

	/**
	 * The owner of the pet
	 */
	private net.minecraft.world.entity.LivingEntity owner;

	/**
	 * Create a new pathfinder goal for the given entity with the given follow speed
	 * and a maximum distance threshold from the owner
	 *
	 * @param pet
	 * @param speed
	 * @param distanceFromOwner
	 */
	public PathfinderGoalPet_Modern(final Mob pet, final double speed, final double distanceFromOwner) {
		super(pet, Goal.Flag.MOVE);

		this.speed = speed;
		this.navigationDistanceThreshold = distanceFromOwner;
	}

	@Override
	public boolean canNavigate() {

		// Update owner
		final CraftEntity bukkitEntity = this.getEntity().getBukkitEntity();

		if (!CompMetadata.hasMetadata(bukkitEntity, "PetOwner"))
			return false;

		final UUID ownerId = UUID.fromString(CompMetadata.getMetadata(bukkitEntity, "PetOwner"));
		final Player ownerPlayer = Remain.getPlayerByUUID(ownerId);

		if (ownerPlayer == null || !ownerPlayer.isOnline())
			return false;

		this.owner = ((CraftPlayer) ownerPlayer).getHandle();

		// Pet is out of reach from the owner
		if (this.owner.distanceTo(this.getEntity()) > this.navigationDistanceThreshold * this.navigationDistanceThreshold) {
			this.getEntity().absMoveTo(this.owner.getX(), this.owner.getY(), this.owner.getZ());

			System.out.println("out of reach (distance = " + this.navigationDistanceThreshold + ")");
			return false;
		}

		/*final Vec3D vec = RandomPositionGenerator.a((EntityCreature) this.getEntity(), 16, 7, this.owner.getPositionVector());

		// If the entity is in the air, remove
		if (vec == null) {
			System.out.println("Pet is in the air/stuck, cannot navigate");

			return false;
		}*/

		// Follow the owner
		return true;
	}

	@Override
	public void onNavigationTick() {

		//System.out.println("@onNavigationTick");

		// Go to this position
		this.getEntity().getNavigation().moveTo(this.owner.getX(), this.owner.getY(), this.owner.getZ(), this.speed);
	}

	@Override
	public boolean canContinueNavigation() {

		//System.out.println("@canContinueNavigation");

		// If we did not make it to the owner's location and still far away, continue calling onNavigation
		return !this.getEntity().getNavigation().isDone() // "GPS" pathfinding
				&& this.owner.distanceTo(this.getEntity()) < this.navigationDistanceThreshold * this.navigationDistanceThreshold;
	}

	@Override
	public void onNavigationStop() {
		// Empty
	}

	static void applyTo(final LivingEntity entity) {
		final CraftWolf wolf = (CraftWolf) entity;
		final Wolf nmsWolf = wolf.getHandle();

		nmsWolf.goalSelector.addGoal(0, new FloatGoal(nmsWolf)); // stay on the top of water
		nmsWolf.targetSelector.addGoal(1, new LookAtPlayerGoal(nmsWolf, ServerPlayer.class, 10));
		nmsWolf.targetSelector.addGoal(2, new PathfinderGoalPet_Modern(nmsWolf, 1, 15));
	}
}
