package org.mineacademy.nms.specific.goal;

/*
final class PathfinderGoalPet_v1_17 extends SimplePathfinderGoal_v1_17 {


	private final double speed;


	private final double navigationDistanceThreshold;


	private EntityLiving owner;


	public PathfinderGoalPet_v1_17(final EntityInsentient pet, final double speed, final double distanceFromOwner) {
		super(pet, PathfinderGoal.Type.a);

		this.speed = speed;
		this.navigationDistanceThreshold = distanceFromOwner;
	}

	@Override
	public boolean canNavigate() {

		// Update owner
		final CraftEntity bukkitEntity = this.getEntity().getBukkitEntity();

		if (!CompMetadata.hasMetadata(bukkitEntity, "PetOwner"))
			return false;

		final UUID ownerId = UUID.fromString(CompMetadata.getMetadata(bukkitEntity, "PetOwner"));
		final Player ownerPlayer = Remain.getPlayerByUUID(ownerId);

		if (ownerPlayer == null || !ownerPlayer.isOnline())
			return false;

		this.owner = ((CraftPlayer) ownerPlayer).getHandle();

		// Pet is out of reach from the owner
		if (this.owner.f(this.getEntity()) > this.navigationDistanceThreshold * this.navigationDistanceThreshold) {
			this.getEntity().setPosition(this.owner.locX(), this.owner.locY(), this.owner.locZ());

			System.out.println("out of reach (distance = " + this.navigationDistanceThreshold + ")");
			return false;
		}

		// Follow the owner
		return true;
	}

	@Override
	public void onNavigationTick() {

		//System.out.println("@onNavigationTick");

		// Go to this position
		this.getEntity().getNavigation().a(this.owner.locX(), this.owner.locY(), this.owner.locZ(), this.speed);
	}

	@Override
	public boolean canContinueNavigation() {

		//System.out.println("@canContinueNavigation");

		// If we did not make it to the owner's location and still far away, continue calling onNavigation
		return !this.getEntity().getNavigation().m() // "GPS" pathfinding
				&& this.owner.f(this.getEntity()) < this.navigationDistanceThreshold * this.navigationDistanceThreshold;
	}

	@Override
	public void onNavigationStop() {
		// Empty
	}

	static void applyTo(final LivingEntity entity) {
		final CraftWolf wolf = (CraftWolf) entity;
		final EntityWolf nmsWolf = wolf.getHandle();

		nmsWolf.bP.a(0, new PathfinderGoalFloat(nmsWolf)); // stay on the top of water
		nmsWolf.bP.a(1, new PathfinderGoalLookAtPlayer(nmsWolf, EntityPlayer.class, 10));
		nmsWolf.bP.a(2, new PathfinderGoalPet_v1_17(nmsWolf, 1, 15));
	}
}
*/