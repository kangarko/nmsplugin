package org.mineacademy.nms.specific.goal;

import java.util.UUID;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftWolf;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_8_R3.EntityCreature;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.EntityWolf;
import net.minecraft.server.v1_8_R3.PathfinderGoalFloat;
import net.minecraft.server.v1_8_R3.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R3.RandomPositionGenerator;
import net.minecraft.server.v1_8_R3.Vec3D;

/**
 * Represents a custom pathfinder goal to follow an owner for pets.
 */
final class PathfinderGoalPet_v1_8 extends SimplePathfinderGoal_v1_8 {

	/**
	 * How fast should the pet walk to the owner
	 */
	private final double speed;

	/**
	 * The maximum distance the pet should walk from.
	 * If the pet is farther than this, we'll teleport it.
	 */
	private final double navigationDistanceThreshold;

	/**
	 * The owner of the pet
	 */
	private EntityLiving owner;

	/**
	 * Create a new pathfinder goal for the given entity with the given follow speed
	 * and a maximum distance threshold from the owner
	 *
	 * @param pet
	 * @param speed
	 * @param distanceFromOwner
	 */
	public PathfinderGoalPet_v1_8(final EntityInsentient pet, final double speed, final double distanceFromOwner) {
		super(pet, 1);

		this.speed = speed;
		this.navigationDistanceThreshold = distanceFromOwner;
	}

	@Override
	public boolean canNavigate() {

		// Update owner
		final CraftEntity bukkitEntity = this.getEntity().getBukkitEntity();

		if (!CompMetadata.hasMetadata(bukkitEntity, "PetOwner"))
			return false;

		final UUID ownerId = UUID.fromString(CompMetadata.getMetadata(bukkitEntity, "PetOwner"));
		final Player ownerPlayer = Remain.getPlayerByUUID(ownerId);

		if (ownerPlayer == null || !ownerPlayer.isOnline())
			return false;

		this.owner = ((CraftPlayer) ownerPlayer).getHandle();

		// Pet is out of reach from the owner
		if (this.owner.h(this.getEntity()) > this.navigationDistanceThreshold * this.navigationDistanceThreshold) {
			this.getEntity().setPosition(this.owner.locX, this.owner.locY, this.owner.locZ);

			System.out.println("out of reach (distance = " + this.navigationDistanceThreshold + ")");
			return false;
		}

		final Vec3D vec = RandomPositionGenerator.a((EntityCreature) this.getEntity(), 16, 7, this.owner.ap());

		// If the entity is in the air, remove
		if (vec == null) {
			System.out.println("Pet is in the air/stuck, cannot navigate");

			return false;
		}

		// Follow the owner
		return true;
	}

	@Override
	public void onNavigationTick() {

		//System.out.println("@onNavigationTick");

		// Go to this position
		this.getEntity().getNavigation().a(this.owner.locX, this.owner.locY, this.owner.locZ, this.speed);
	}

	@Override
	public boolean canContinueNavigation() {

		//System.out.println("@canContinueNavigation");

		// If we did not make it to the owner's location and still far away, continue calling onNavigation
		return !this.getEntity().getNavigation().m() // "GPS" pathfinding
				&& this.owner.h(this.getEntity()) < this.navigationDistanceThreshold * this.navigationDistanceThreshold;
	}

	@Override
	public void onNavigationStop() {
		// Empty
	}

	static void applyTo(final LivingEntity entity) {
		final CraftWolf wolf = (CraftWolf) entity;
		final EntityWolf nmsWolf = wolf.getHandle();

		nmsWolf.goalSelector.a(0, new PathfinderGoalFloat(nmsWolf)); // stay on the top of water
		nmsWolf.goalSelector.a(1, new PathfinderGoalLookAtPlayer(nmsWolf, EntityPlayer.class, 10));
		nmsWolf.goalSelector.a(2, new PathfinderGoalPet_v1_8(nmsWolf, 1, 15));
	}
}
