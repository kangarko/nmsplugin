package org.mineacademy.nms.specific.goal;

import java.util.EnumSet;

import lombok.Getter;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.Goal;

/**
 * A helping in-the-middle class to make custom pathfinding easier.
 */
abstract class SimplePathfinderGoal_Modern extends Goal implements PathfinderGoalProvider {

	/**
	 * The controlled entity
	 */
	@Getter
	private final Mob entity;

	/**
	 * Create a new pathfinder goal for the given entity and the goal type
	 */
	protected SimplePathfinderGoal_Modern(final Mob entity, final Goal.Flag goalTypes) {
		this.entity = entity;

		// Set pathfinder goals
		this.setFlags(EnumSet.of(goalTypes));
	}

	// See canNavigate
	@Override
	public final boolean canUse() {
		return this.canNavigate();
	}

	// See onNavigationTick
	@Override
	public final void tick() {
		super.tick();

		this.onNavigationTick();
	}

	// See canContinueNavigation
	@Override
	public final boolean canContinueToUse() {
		return super.canContinueToUse() && this.canContinueNavigation();
	}

	// See onNavigationStop

	/**
	 * @see Goal#stop()
	 */
	@Override
	public final void stop() {
		super.stop();

		this.onNavigationStop();
	}

}
