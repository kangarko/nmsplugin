package org.mineacademy.nms.specific.goal;

import java.util.EnumSet;

import lombok.Getter;
import net.minecraft.server.v1_16_R3.EntityInsentient;
import net.minecraft.server.v1_16_R3.PathfinderGoal;

/**
 * A helping in-the-middle class to make custom pathfinding easier.
 */
abstract class SimplePathfinderGoal_v1_16 extends PathfinderGoal implements PathfinderGoalProvider {

	/**
	 * The controlled entity
	 */
	@Getter
	private final EntityInsentient entity;

	/**
	 * Create a new pathfinder goal for the given entity and the goal type
	 */
	protected SimplePathfinderGoal_v1_16(final EntityInsentient entity, final Type goalTypes) {
		this.entity = entity;

		// Set pathfinder goals
		this.a(EnumSet.of(goalTypes));
	}

	// See canNavigate
	@Override
	public final boolean a() {
		return this.canNavigate();
	}

	// See onNavigationTick
	@Override
	public final void c() {
		super.c();

		this.onNavigationTick();
	}

	// See canContinueNavigation
	@Override
	public final boolean b() {
		return super.b() && this.canContinueNavigation();
	}

	// See onNavigationStop

	/**
	 * @see net.minecraft.server.v1_16_R3.PathfinderGoal#d()
	 */
	@Override
	public final void d() {
		super.d();

		this.onNavigationStop();
	}

}
