package org.mineacademy.nms.specific.goal;

import org.bukkit.entity.LivingEntity;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.exception.FoException;

public interface PathfinderGoalProvider {

	static void addPetNavigationTo(final LivingEntity entity) {
		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			PathfinderGoalPet_v1_8.applyTo(entity);

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
			PathfinderGoalPet_v1_16.applyTo(entity);

		else if ((MinecraftVersion.equals(MinecraftVersion.V.v1_20) && MinecraftVersion.getSubversion() >= 5) || MinecraftVersion.newerThan(MinecraftVersion.V.v1_20))
			PathfinderGoalPet_Modern.applyTo(entity);

		else
			throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());
	}

	boolean canNavigate();

	/**
	 * Called when {@link #canNavigate()} is true
	 */
	void onNavigationTick();

	/**
	 * Checks if we can continue calling {@link #onNavigationTick()}
	 *
	 * @return
	 */
	boolean canContinueNavigation();

	/**
	 * Executed if {@link #canContinueNavigation()} returns false
	 */
	void onNavigationStop();
}
