/*package org.mineacademy.nms.specific.goal;

import java.util.EnumSet;

import lombok.Getter;
import net.minecraft.world.entity.EntityInsentient;
import net.minecraft.world.entity.ai.goal.PathfinderGoal;


abstract class SimplePathfinderGoal_v1_17 extends PathfinderGoal implements PathfinderGoalProvider {


	@Getter
	private final EntityInsentient entity;


	protected SimplePathfinderGoal_v1_17(final EntityInsentient entity, final PathfinderGoal.Type goalTypes) {
		this.entity = entity;

		// Set pathfinder goals
		this.a(EnumSet.of(goalTypes));
	}

	// See canNavigate
	@Override
	public final boolean a() {
		return this.canNavigate();
	}

	// See onNavigationTick
	@Override
	public final void c() {
		super.c();

		this.onNavigationTick();
	}

	// See canContinueNavigation
	@Override
	public final boolean b() {
		return super.b() && this.canContinueNavigation();
	}

	// See onNavigationStop


	@Override
	public final void d() {
		super.d();

		this.onNavigationStop();
	}

}*/
