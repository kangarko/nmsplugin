package org.mineacademy.nms.specific.npc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.npc.NPCRegistry;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.DataWatcher;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.minecraft.server.v1_8_R3.WorldServer;

/**
 * Represents a human NPC.
 */
@Getter
final class NPC_v1_8 implements NPC {

	/**
	 * The name of the NPC, also controls its skin
	 */
	private final String name;

	/**
	 * The skin of the NPC, defaults to its name if not set
	 */
	private String skin;

	/**
	 * Shall this NPC look at the nearest player?
	 */
	@Setter
	private boolean lookingAtPlayer;

	/**
	 * The spawned NMS entity
	 */
	private EntityPlayer spawnedNPC;

	/**
	 * To whom this entity is visiblee
	 */
	private final Set<UUID> viewers = new HashSet<>();

	/**
	 * Create a new NPC with the given name.
	 * @param name
	 */
	public NPC_v1_8(final String name) {
		this.name = name;
		this.skin = name;
	}

	/**
	 * Spawns the NPC as entity at the given location. The NPC is invisible
	 * and must be shown manually using the show(...) methods.
	 *
	 * @param location
	 */
	@Override
	public void spawn(final Location location) {
		Valid.checkBoolean(!this.isSpawned(), this + " already exists!");

		final MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		final WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
		final GameProfile profile = new GameProfile(UUID.randomUUID(), this.name);

		final EntityPlayer entity = new EntityPlayer(server, world, profile, new PlayerInteractManager(world));

		entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

		this.spawnedNPC = entity;

		NPCRegistry.getInstance().register(this);
	}

	/**
	 * Show the NPC to the given player, optionally showing in tab list
	 *
	 * @param player
	 * @param showInTab
	 */
	@Override
	public void show(final Player player, final boolean showInTab) {
		Valid.checkBoolean(!this.isVisibleTo(player), this + " is already visible to " + player.getName());

		this.sendPacket(player,
				new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, spawnedNPC),
				new PacketPlayOutNamedEntitySpawn(spawnedNPC),
				new PacketPlayOutEntityHeadRotation(spawnedNPC, (byte) (spawnedNPC.yaw * 256 / 360)));

		if (!showInTab)
			Common.runLater(20, () -> this.sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, spawnedNPC)));

		this.viewers.add(player.getUniqueId());
	}

	/**
	 * Hide the NPC from the given player
	 *
	 * @param player
	 */
	@Override
	public void hide(final Player player) {
		Valid.checkBoolean(this.isVisibleTo(player), "Player " + player.getName() + " cannot see " + this);

		this.sendPacket(player, new PacketPlayOutEntityDestroy(this.spawnedNPC.getId()));
		this.viewers.remove(player.getUniqueId());
	}

	/**
	 * Makes this NPC look at the given target location, can be another entity
	 *
	 * @param targetLocation
	 */
	@Override
	public void setRotation(final Location targetLocation) {
		final Location entityLoc = this.getLocation();
		final Vector direction = entityLoc.toVector().subtract(targetLocation.toVector()).normalize();

		final double yaw = 180 - Math.toDegrees(Math.atan2(direction.getX(), direction.getZ()));
		final double pitch = 90 - Math.toDegrees(Math.acos(direction.getY()));

		this.sendPacketsToViewers(
				new PacketPlayOutEntityHeadRotation(this.spawnedNPC, (byte) (yaw * 256 / 360)),
				new PacketPlayOutEntityLook(this.spawnedNPC.getId(), (byte) (yaw * 256 / 360), (byte) (pitch * 256 / 360), true));
	}

	/**
	 * Changes the skin of this NPC
	 *
	 * @param skinName
	 */
	@Override
	public void setSkin(final String skinName) {

		// To change the skin we must redraw the NPC
		final Set<UUID> oldViewers = new HashSet<>();
		oldViewers.addAll(this.viewers);

		this.hide();

		try {
			final URLConnection connection = new URL(String.format("https://api.ashcon.app/mojang/v2/user/%s", skinName)).openConnection();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			final String output = String.join("", reader.lines().collect(Collectors.toList()));

			final SerializedMap json = SerializedMap.fromJson(output);
			final SerializedMap raw = json.getMap("textures").getMap("raw");

			this.spawnedNPC.getProfile().getProperties().put("textures", new Property("textures", raw.getString("value"), raw.getString("signature")));

		} catch (final IOException ex) {
			Common.error(ex, "Unable to set skin to " + skinName + " for " + this);
		}

		final DataWatcher watcher = this.spawnedNPC.getDataWatcher();
		//watcher.set(new DataWatcherObject<>(16, DataWatcherRegistry.a), (byte) 127);

		for (final UUID oldViewer : oldViewers) {
			final Player viewer = Remain.getPlayerByUUID(oldViewer);

			if (viewer != null) {
				this.sendPacket(viewer, new PacketPlayOutEntityMetadata(this.spawnedNPC.getId(), watcher, true));
				this.show(viewer);
			}
		}

		this.skin = skinName;
	}

	/*
	 * Send the packets to everyone viewing this entity
	 */
	private void sendPacketsToViewers(final Packet<?>... packets) {
		for (final UUID viewerUid : this.viewers) {
			final Player player = Remain.getPlayerByUUID(viewerUid);

			this.sendPacket(player, packets);
		}
	}

	/*
	 * Send the given packets to the given player
	 */
	private void sendPacket(final Player player, final Packet<?>... packets) {
		final PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;

		for (final Packet<?> packet : packets)
			connection.sendPacket(packet);
	}

	@Override
	public UUID getUniqueId() {
		return this.spawnedNPC.getUniqueID();
	}

	/**
	 * Convenience method to return the location of this NPC.
	 *
	 * @return
	 */
	@Override
	public Location getLocation() {
		Valid.checkBoolean(this.isSpawned(), "Cannot call getLocation when " + this + " is not spawned");

		return this.spawnedNPC.getBukkitEntity().getLocation();
	}

	@Override
	public String toString() {
		return "NPC{id=" + (this.isSpawned() ? this.spawnedNPC.getId() : "not spawned") + ", name=" + this.name + "}";
	}

	/**
	 * @see org.mineacademy.fo.model.ConfigSerializable#serialize()
	 */
	@Override
	public SerializedMap serialize() {
		Valid.checkBoolean(this.isSpawned(), "Cannot save non-spawned NPCs");

		return SerializedMap.ofArray(
				"UUID", this.spawnedNPC.getUniqueID(),
				"Name", this.name,
				"Skin", this.skin,
				"Looking_At_Player", this.lookingAtPlayer,
				"Last_Location", this.getLocation());
	}

	/**
	 * Converts information saved in data.db file as a map into an NPC,
	 * also spawning it. After spawn this NPC will auto register in {@link NPCRegistry}
	 *
	 * @param map
	 * @return
	 */
	public static NPC_v1_8 deserialize(final SerializedMap map) {
		final String name = map.getString("Name");
		final String skin = map.getString("Skin");
		final boolean lookingAtPlayer = map.getBoolean("Looking_At_Player");
		final Location lastLocation = map.getLocation("Last_Location");

		final NPC_v1_8 npc = new NPC_v1_8(name);

		npc.spawn(lastLocation);
		npc.setSkin(skin);
		npc.setLookingAtPlayer(lookingAtPlayer);

		return npc;
	}
}
