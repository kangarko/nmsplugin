package org.mineacademy.nms.specific.npc;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.*;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.network.syncher.SynchedEntityData.DataValue;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ClientInformation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.CraftServer;
import org.bukkit.craftbukkit.CraftWorld;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.util.Vector;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.npc.NPCRegistry;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents a human NPC.
 */
@Getter
final class NPC_Modern implements NPC {

	/**
	 * The name of the NPC, also controls its skin
	 */
	private final String name;

	/**
	 * The skin of the NPC, defaults to its name if not set
	 */
	private String skin;

	/**
	 * Shall this NPC look at the nearest player?
	 */
	@Setter
	private boolean lookingAtPlayer;

	/**
	 * The spawned NMS entity
	 */
	private ServerPlayer spawnedNPC;

	/**
	 * To whom this entity is visiblee
	 */
	private final Set<UUID> viewers = new HashSet<>();

	/**
	 * Create a new NPC with the given name.
	 *
	 * @param name
	 */
	public NPC_Modern(final String name) {
		this.name = name;
		this.skin = name;
	}

	/**
	 * Spawns the NPC as entity at the given location. The NPC is invisible
	 * and must be shown manually using the show(...) methods.
	 *
	 * @param location
	 */
	@Override
	public void spawn(final Location location) {
		Valid.checkBoolean(!this.isSpawned(), this + " already exists!");

		final MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		final ServerLevel world = ((CraftWorld) location.getWorld()).getHandle();
		final GameProfile profile = new GameProfile(UUID.randomUUID(), this.name);

		final ServerPlayer entity = new ServerPlayer(server, world, profile, ClientInformation.createDefault());

		entity.absMoveTo(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

		this.spawnedNPC = entity;

		NPCRegistry.getInstance().register(this);
	}

	void openAnvil() {
		Bukkit.createInventory(null, InventoryType.ANVIL, "My Anvil Inventory");
	}

	/**
	 * Show the NPC to the given player, optionally showing in tab list
	 *
	 * @param player
	 * @param showInTab
	 */
	@Override
	public void show(final Player player, final boolean showInTab) {
		Valid.checkBoolean(!this.isVisibleTo(player), this + " is already visible to " + player.getName());

		// Ugly hack
		this.spawnedNPC.connection = ((CraftPlayer) player).getHandle().connection;

		this.sendPacket(player,
				new ClientboundPlayerInfoUpdatePacket(ClientboundPlayerInfoUpdatePacket.Action.ADD_PLAYER, spawnedNPC),
				new ClientboundAddEntityPacket(spawnedNPC, 0, spawnedNPC.blockPosition()),
				new ClientboundRotateHeadPacket(spawnedNPC, (byte) (spawnedNPC.getBukkitYaw() * 256 / 360)));

		if (!showInTab)
			Common.runLater(20, () -> this.sendPacket(player, new ClientboundPlayerInfoRemovePacket(Arrays.asList(spawnedNPC.getUUID()))));

		this.updateMetadata(player);

		this.viewers.add(player.getUniqueId());
	}

	private void updateMetadata(final Player player) {
		final SynchedEntityData dataWatcher = this.spawnedNPC.getEntityData();
		final EntityDataAccessor<Byte> dataAccessor = new EntityDataAccessor<>(0, EntityDataSerializers.BYTE);

		dataWatcher.set(dataAccessor, (byte) (dataWatcher.get(dataAccessor) & ~(1 << 1)));
		dataWatcher.markDirty(dataAccessor);

		List<DataValue<?>> list = dataWatcher.packDirty();

		if (list == null)
			list = new ArrayList<>();

		this.sendPacket(player, new ClientboundSetEntityDataPacket(this.spawnedNPC.getId(), list));
	}

	/**
	 * Hide the NPC from the given player
	 *
	 * @param player
	 */
	@Override
	public void hide(final Player player) {
		Valid.checkBoolean(this.isVisibleTo(player), "Player " + player.getName() + " cannot see " + this);

		this.sendPacket(player, new ClientboundRemoveEntitiesPacket(this.spawnedNPC.getId()));
		this.viewers.remove(player.getUniqueId());
	}

	/**
	 * Makes this NPC look at the given target location, can be another entity
	 *
	 * @param targetLocation
	 */
	@Override
	public void setRotation(final Location targetLocation) {
		final Location entityLoc = this.getLocation();
		final Vector direction = entityLoc.toVector().subtract(targetLocation.toVector()).normalize();

		final double yaw = 180 - Math.toDegrees(Math.atan2(direction.getX(), direction.getZ()));
		final double pitch = 90 - Math.toDegrees(Math.acos(direction.getY()));

		this.sendPacketsToViewers(
				new ClientboundRotateHeadPacket(this.spawnedNPC, (byte) (yaw * 256 / 360)),
				new ClientboundMoveEntityPacket.Rot(this.spawnedNPC.getId(), (byte) (yaw * 256 / 360), (byte) (pitch * 256 / 360), true));
	}

	/**
	 * Changes the skin of this NPC
	 *
	 * @param skinName
	 */
	@Override
	public void setSkin(final String skinName) {

		// To change the skin we must redraw the NPC
		final Set<UUID> oldViewers = new HashSet<>();
		oldViewers.addAll(this.viewers);

		this.hide();

		try {
			final URLConnection connection = new URL(String.format("https://api.ashcon.app/mojang/v2/user/%s", skinName)).openConnection();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			final String output = String.join("", reader.lines().collect(Collectors.toList()));

			final SerializedMap json = SerializedMap.fromJson(output);
			final SerializedMap raw = json.getMap("textures").getMap("raw");

			this.spawnedNPC.getGameProfile().getProperties().put("textures", new Property("textures", raw.getString("value"), raw.getString("signature")));

		} catch (final FileNotFoundException ex) {
			// Invalid skin

		} catch (final IOException ex) {
			Common.error(ex, "Unable to set skin to " + skinName + " for " + this);
		}

		for (final UUID oldViewer : oldViewers) {
			final Player viewer = Remain.getPlayerByUUID(oldViewer);

			if (viewer != null)
				this.show(viewer);
		}

		this.skin = skinName;
	}

	/*
	 * Send the packets to everyone viewing this entity
	 */
	private void sendPacketsToViewers(final Packet<?>... packets) {
		for (final UUID viewerUid : this.viewers) {
			final Player player = Remain.getPlayerByUUID(viewerUid);

			this.sendPacket(player, packets);
		}
	}

	/*
	 * Send the given packets to the given player
	 */
	private void sendPacket(final Player player, final Packet<?>... packets) {
		final ServerGamePacketListenerImpl connection = ((CraftPlayer) player).getHandle().connection;

		for (final Packet<?> packet : packets)
			connection.send(packet);
	}

	@Override
	public UUID getUniqueId() {
		return this.spawnedNPC.getUUID();
	}

	/**
	 * Convenience method to return the location of this NPC.
	 *
	 * @return
	 */
	@Override
	public Location getLocation() {
		Valid.checkBoolean(this.isSpawned(), "Cannot call getLocation when " + this + " is not spawned");

		return this.spawnedNPC.getBukkitEntity().getLocation();
	}

	@Override
	public String toString() {
		return "NPC{id=" + (this.isSpawned() ? this.spawnedNPC.getId() : "not spawned") + ", name=" + this.name + "}";
	}

	/**
	 * @see org.mineacademy.fo.model.ConfigSerializable#serialize()
	 */
	@Override
	public SerializedMap serialize() {
		Valid.checkBoolean(this.isSpawned(), "Cannot save non-spawned NPCs");

		return SerializedMap.ofArray(
				"UUID", this.spawnedNPC.getUUID(),
				"Name", this.name,
				"Skin", this.skin,
				"Looking_At_Player", this.lookingAtPlayer,
				"Last_Location", this.getLocation());
	}

	/**
	 * Converts information saved in data.db file as a map into an NPC,
	 * also spawning it. After spawn this NPC will auto register in {@link NPCRegistry}
	 *
	 * @param map
	 * @return
	 */
	public static NPC_Modern deserialize(final SerializedMap map) {
		final String name = map.getString("Name");
		final String skin = map.getString("Skin");
		final boolean lookingAtPlayer = map.getBoolean("Looking_At_Player");
		final Location lastLocation = map.getLocation("Last_Location");

		final NPC_Modern npc = new NPC_Modern(name);

		npc.spawn(lastLocation);
		npc.setSkin(skin);
		npc.setLookingAtPlayer(lookingAtPlayer);

		return npc;
	}
}
