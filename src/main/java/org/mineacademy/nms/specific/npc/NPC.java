package org.mineacademy.nms.specific.npc;

import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.model.ConfigSerializable;
import org.mineacademy.fo.remain.Remain;

public interface NPC extends ConfigSerializable {

	static NPC create(final String name) {
		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			return new NPC_v1_8(name);

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
			return new NPC_v1_16(name);

		else if ((MinecraftVersion.equals(MinecraftVersion.V.v1_20) && MinecraftVersion.getSubversion() >= 5) || MinecraftVersion.newerThan(MinecraftVersion.V.v1_20))
			return new NPC_Modern(name);

		throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());
	}

	static NPC deserialize(final SerializedMap map) {
		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			return NPC_v1_16.deserialize(map);

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
			return NPC_v1_16.deserialize(map);

		else if ((MinecraftVersion.equals(MinecraftVersion.V.v1_20) && MinecraftVersion.getSubversion() >= 5) || MinecraftVersion.newerThan(MinecraftVersion.V.v1_20))
			return NPC_Modern.deserialize(map);

		throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());
	}

	String getName();

	String getSkin();

	UUID getUniqueId();

	boolean isLookingAtPlayer();

	void setLookingAtPlayer(boolean looking);

	Object getSpawnedNPC();

	Set<UUID> getViewers();

	Location getLocation();

	void setRotation(Location targetLocation);

	void setSkin(String skinName);

	void spawn(Location location);

	/**
	 * Return if this NPC is spawned
	 *
	 * @return
	 */
	default boolean isSpawned() {
		return this.getSpawnedNPC() != null;
	}

	/**
	 * Return if the given player can see this NPC
	 *
	 * @param player
	 * @return
	 */
	default boolean isVisibleTo(final Player player) {
		Valid.checkBoolean(this.isSpawned(), "Cannot call isVisibleTo when " + this + " is not spawned");

		return this.getViewers().contains(player.getUniqueId());
	}

	/**
	 * Show the NPC to all players, hidden from tab list
	 */
	default void show() {
		this.show(false);
	}

	/**
	 * Show the NPC to all players, optionally showing in tab list
	 *
	 * @param showInTab
	 */
	default void show(final boolean showInTab) {
		for (final Player online : Remain.getOnlinePlayers())
			if (!this.isVisibleTo(online))
				this.show(online);
	}

	/**
	 * Show the NPC to the given player, hidden from tab list
	 *
	 * @param player
	 */
	default void show(final Player player) {
		this.show(player, false);
	}

	void show(Player player, boolean showInTab);

	/**
	 * Hide the NPC from all players
	 */
	default void hide() {
		Valid.checkBoolean(this.isSpawned(), this + " does not exist!");

		for (final Player online : Remain.getOnlinePlayers())
			if (this.isVisibleTo(online))
				this.hide(online);
	}

	/**
	 * Manually remove the given viewer from viewers list without sending a packet.
	 * Internal use only.
	 *
	 * @param viewerUid
	 */
	default void removeViewer(final UUID viewerUid) {
		this.getViewers().remove(viewerUid);
	}

	void hide(Player player);
}
