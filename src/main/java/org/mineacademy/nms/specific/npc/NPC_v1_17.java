/*package org.mineacademy.nms.specific.npc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_17_R1.CraftServer;
import org.bukkit.craftbukkit.v1_17_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_17_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.nms.npc.NPCRegistry;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.PacketPlayOutEntity;
import net.minecraft.network.protocol.game.PacketPlayOutEntityDestroy;
import net.minecraft.network.protocol.game.PacketPlayOutEntityHeadRotation;
import net.minecraft.network.protocol.game.PacketPlayOutEntityMetadata;
import net.minecraft.network.protocol.game.PacketPlayOutNamedEntitySpawn;
import net.minecraft.network.protocol.game.PacketPlayOutPlayerInfo;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.EntityPlayer;
import net.minecraft.server.level.WorldServer;
import net.minecraft.server.network.PlayerConnection;


@Getter
final class NPC_v1_17 implements NPC {


	private final String name;


	private String skin;


	@Setter
	private boolean lookingAtPlayer;


	private EntityPlayer spawnedNPC;


	private final Set<UUID> viewers = new HashSet<>();


	public NPC_v1_17(final String name) {
		this.name = name;
		this.skin = name;
	}


	@Override
	public void spawn(final Location location) {
		Valid.checkBoolean(!this.isSpawned(), this + " already exists!");

		final MinecraftServer server = ((CraftServer) Bukkit.getServer()).getServer();
		final WorldServer world = ((CraftWorld) location.getWorld()).getHandle();
		final GameProfile profile = new GameProfile(UUID.randomUUID(), this.name);

		final EntityPlayer entity = new EntityPlayer(server, world, profile);

		entity.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

		this.spawnedNPC = entity;

		NPCRegistry.getInstance().register(this);
	}


	@Override
	public void show(final Player player, final boolean showInTab) {
		Valid.checkBoolean(!this.isVisibleTo(player), this + " is already visible to " + player.getName());

		this.sendPacket(player,
				new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.a, spawnedNPC),
				new PacketPlayOutNamedEntitySpawn(spawnedNPC),
				new PacketPlayOutEntityHeadRotation(spawnedNPC, (byte) (spawnedNPC.getBukkitYaw() * 256 / 360)));

		if (!showInTab)
			Common.runLater(20, () -> this.sendPacket(player, new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.e, spawnedNPC)));

		this.updateMetadata(player);

		this.viewers.add(player.getUniqueId());
	}

	private void updateMetadata(final Player player) {
		final DataWatcher dataWatcher = this.spawnedNPC.getDataWatcher();
		final DataWatcher.Item<Byte> watchedByte = ((Map<Integer, DataWatcher.Item<Byte>>) ReflectionUtil.getFieldContent(dataWatcher, "f")).get(0);

		watchedByte.a((byte) (watchedByte.b() & (~(1 << 1))));

		this.sendPacket(player, new PacketPlayOutEntityMetadata(this.spawnedNPC.getId(), dataWatcher, true));
	}


	@Override
	public void hide(final Player player) {
		Valid.checkBoolean(this.isVisibleTo(player), "Player " + player.getName() + " cannot see " + this);

		this.sendPacket(player, new PacketPlayOutEntityDestroy(this.spawnedNPC.getId()));
		this.viewers.remove(player.getUniqueId());
	}


	@Override
	public void setRotation(final Location targetLocation) {
		final Location entityLoc = this.getLocation();
		final Vector direction = entityLoc.toVector().subtract(targetLocation.toVector()).normalize();

		final double yaw = 180 - Math.toDegrees(Math.atan2(direction.getX(), direction.getZ()));
		final double pitch = 90 - Math.toDegrees(Math.acos(direction.getY()));

		this.sendPacketsToViewers(
				new PacketPlayOutEntityHeadRotation(this.spawnedNPC, (byte) (yaw * 256 / 360)),
				new PacketPlayOutEntity.PacketPlayOutEntityLook(this.spawnedNPC.getId(), (byte) (yaw * 256 / 360), (byte) (pitch * 256 / 360), true));
	}


	@Override
	public void setSkin(final String skinName) {

		// To change the skin we must redraw the NPC
		final Set<UUID> oldViewers = new HashSet<>();
		oldViewers.addAll(this.viewers);

		this.hide();

		try {
			final URLConnection connection = new URL(String.format("https://api.ashcon.app/mojang/v2/user/%s", skinName)).openConnection();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			final String output = String.join("", reader.lines().collect(Collectors.toList()));

			final SerializedMap json = SerializedMap.fromJson(output);
			final SerializedMap raw = json.getMap("textures").getMap("raw");

			this.spawnedNPC.getProfile().getProperties().put("textures", new Property("textures", raw.getString("value"), raw.getString("signature")));

		} catch (final IOException ex) {
			Common.error(ex, "Unable to set skin to " + skinName + " for " + this);
		}

		for (final UUID oldViewer : oldViewers) {
			final Player viewer = Remain.getPlayerByUUID(oldViewer);

			if (viewer != null)
				this.show(viewer);
		}

		this.skin = skinName;
	}

	private void sendPacketsToViewers(final Packet<?>... packets) {
		for (final UUID viewerUid : this.viewers) {
			final Player player = Remain.getPlayerByUUID(viewerUid);

			this.sendPacket(player, packets);
		}
	}

	private void sendPacket(final Player player, final Packet<?>... packets) {
		final PlayerConnection connection = ((CraftPlayer) player).getHandle().b;

		for (final Packet<?> packet : packets)
			connection.sendPacket(packet);
	}

	@Override
	public UUID getUniqueId() {
		return this.spawnedNPC.getUniqueID();
	}


	@Override
	public Location getLocation() {
		Valid.checkBoolean(this.isSpawned(), "Cannot call getLocation when " + this + " is not spawned");

		return this.spawnedNPC.getBukkitEntity().getLocation();
	}

	@Override
	public String toString() {
		return "NPC{id=" + (this.isSpawned() ? this.spawnedNPC.getId() : "not spawned") + ", name=" + this.name + "}";
	}


	@Override
	public SerializedMap serialize() {
		Valid.checkBoolean(this.isSpawned(), "Cannot save non-spawned NPCs");

		return SerializedMap.ofArray(
				"UUID", this.spawnedNPC.getUniqueID(),
				"Name", this.name,
				"Skin", this.skin,
				"Looking_At_Player", this.lookingAtPlayer,
				"Last_Location", this.getLocation());
	}


	public static NPC_v1_17 deserialize(final SerializedMap map) {
		final String name = map.getString("Name");
		final String skin = map.getString("Skin");
		final boolean lookingAtPlayer = map.getBoolean("Looking_At_Player");
		final Location lastLocation = map.getLocation("Last_Location");

		final NPC_v1_17 npc = new NPC_v1_17(name);

		npc.spawn(lastLocation);
		npc.setSkin(skin);
		npc.setLookingAtPlayer(lookingAtPlayer);

		return npc;
	}
}
*/