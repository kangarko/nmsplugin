package org.mineacademy.nms.specific.sign;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.game.ClientboundOpenSignEditorPacket;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.entity.SignText;

public class NMSSign_v1_20 extends NMSSign {

	NMSSign_v1_20(final String... lines) {
		super(lines);
	}

	@Override
	protected void loadSignTextAndOpen(final Location location, final Player player) {

		// Create a fake tile entity (sign)
		// Edit the tile entities lines
		final BlockPos position = new BlockPos(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		final SignBlockEntity sign = new SignBlockEntity(position, Blocks.OAK_SIGN.defaultBlockState());

		final SignText signText = new SignText();

		for (int line = 0; line < this.getLines().length; line++)
			signText.setMessage(line, (Component) Remain.toIChatBaseComponentPlain(this.getLines()[line]));

		signText.setColor(DyeColor.ORANGE);
		sign.setText(signText, true); // isFrontText = true

		// Send the lines packet
		Remain.sendPacket(player, sign.getUpdatePacket());

		// Finally send open editor packet
		Remain.sendPacket(player, new ClientboundOpenSignEditorPacket(position, true /* isFrontText = true */));
	}
}
