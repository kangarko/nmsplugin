package org.mineacademy.nms.specific.sign;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompMaterial;

import lombok.AccessLevel;
import lombok.Getter;

public abstract class NMSSign {

	@Getter(AccessLevel.PROTECTED)
	private final String[] lines;

	NMSSign(final String... lines) {
		Valid.checkBoolean(lines.length == 4, "Lines must be 4 lines long not " + lines.length);

		this.lines = lines;
	}

	public void show(final Player player) {

		// 1) We need to "fake" block creation and tell the player that a sign has been placed
		final Location location = player.getLocation();

		location.setY(location.getY() < 20 ? 100 : 1);

		// Mark player
		player.setMetadata("CustomSign", new FixedMetadataValue(SimplePlugin.getInstance(), ""));

		// Simulate a sign creation
		player.sendBlockChange(location, CompMaterial.OAK_SIGN.getMaterial(), (byte) 0);

		// 2) Then we load the lines text onto that fake sign
		// 3) Finally we open a sign editing packet for that sign at that fake location
		this.loadSignTextAndOpen(location, player);
	}

	protected abstract void loadSignTextAndOpen(Location location, Player player);

	public static void openSign(final Player player, final String... lines) {
		final NMSSign sign;

		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			sign = new NMSSign_v1_8(lines);

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
			sign = new NMSSign_v1_16(lines);

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_20))
			sign = new NMSSign_v1_20(lines);

		else
			throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());

		sign.show(player);
	}
}
