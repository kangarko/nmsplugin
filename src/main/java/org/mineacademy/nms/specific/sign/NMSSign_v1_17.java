/*package org.mineacademy.nms.specific.sign;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.core.BlockPosition;
import net.minecraft.network.chat.IChatBaseComponent;
import net.minecraft.network.protocol.game.PacketPlayOutOpenSignEditor;
import net.minecraft.world.item.EnumColor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.TileEntitySign;

public class NMSSign_v1_17 extends NMSSign {

	NMSSign_v1_17(final String... lines) {
		super(lines);
	}

	@Override
	protected void loadSignTextAndOpen(final Location location, final Player player) {

		// Create a fake tile entity (sign)
		// Edit the tile entities lines
		final BlockPosition position = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		final TileEntitySign sign = new TileEntitySign(position, Blocks.cg.getBlockData());

		for (int line = 0; line < this.getLines().length; line++)
			sign.a(line, (IChatBaseComponent) Remain.toIChatBaseComponentPlain(this.getLines()[line]));

		sign.setColor(EnumColor.a);

		// Send the lines packet
		Remain.sendPacket(player, sign.getUpdatePacket());

		// Finally send open editor packet
		Remain.sendPacket(player, new PacketPlayOutOpenSignEditor(position));
	}
}
*/