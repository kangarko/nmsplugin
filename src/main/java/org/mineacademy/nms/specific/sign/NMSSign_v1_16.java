package org.mineacademy.nms.specific.sign;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_16_R3.BlockPosition;
import net.minecraft.server.v1_16_R3.EnumColor;
import net.minecraft.server.v1_16_R3.IChatBaseComponent;
import net.minecraft.server.v1_16_R3.PacketPlayOutOpenSignEditor;
import net.minecraft.server.v1_16_R3.TileEntitySign;

public class NMSSign_v1_16 extends NMSSign {

	NMSSign_v1_16(final String... lines) {
		super(lines);
	}

	@Override
	protected void loadSignTextAndOpen(final Location location, final Player player) {

		// Create a fake tile entity (sign)
		// Edit the tile entities lines
		final BlockPosition position = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		final TileEntitySign sign = new TileEntitySign();

		//position, Blocks.cg.getBlockData()
		sign.setPosition(position);

		for (int line = 0; line < this.getLines().length; line++)
			sign.a(line, (IChatBaseComponent) Remain.toIChatBaseComponentPlain(this.getLines()[line]));

		sign.setColor(EnumColor.ORANGE);

		// Send the lines packet
		Remain.sendPacket(player, sign.getUpdatePacket());

		// Finally send open editor packet
		Remain.sendPacket(player, new PacketPlayOutOpenSignEditor(position));
	}
}
