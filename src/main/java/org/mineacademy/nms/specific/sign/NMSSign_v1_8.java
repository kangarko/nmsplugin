package org.mineacademy.nms.specific.sign;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenSignEditor;
import net.minecraft.server.v1_8_R3.TileEntitySign;

public class NMSSign_v1_8 extends NMSSign {

	NMSSign_v1_8(final String... lines) {
		super(lines);
	}

	@Override
	protected void loadSignTextAndOpen(final Location location, final Player player) {

		// Create a fake tile entity (sign)
		// Edit the tile entities lines
		final BlockPosition position = new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		final TileEntitySign sign = new TileEntitySign();

		sign.isEditable = false;
		sign.a(position);

		// TODO Always appears empty on 1.8.8
		for (int line = 0; line < this.getLines().length; line++)
			sign.lines[line] = IChatBaseComponent.ChatSerializer.a(this.getLines()[line]);

		// Send the lines packet
		Remain.sendPacket(player, sign.getUpdatePacket());

		// Finally send open editor packet
		Remain.sendPacket(player, new PacketPlayOutOpenSignEditor(position));
	}
}
