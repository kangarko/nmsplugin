package org.mineacademy.nms.specific.hologram;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.server.v1_16_R3.EntityArmorStand;
import net.minecraft.server.v1_16_R3.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_16_R3.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_16_R3.WorldServer;

class NMSHologram_v1_16 extends NMSHologram {

	@Override
	protected Object createEntity(final Object nmsWorld, final Location location) {
		return new EntityArmorStand((WorldServer) nmsWorld, location.getX(), location.getY(), location.getZ());
	}

	@Override
	protected void sendPackets(final Player player, final Object nmsArmorStand) {
		final EntityArmorStand nmsStand = (EntityArmorStand) nmsArmorStand;

		Remain.sendPacket(player, new PacketPlayOutSpawnEntityLiving(nmsStand));
		Remain.sendPacket(player, new PacketPlayOutEntityMetadata(nmsStand.getId(), nmsStand.getDataWatcher(), true));
	}
}
