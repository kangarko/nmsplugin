package org.mineacademy.nms.specific.hologram;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.remain.Remain;

import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.network.protocol.game.ClientboundSetEntityDataPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.decoration.ArmorStand;

class NMSHologram_Modern extends NMSHologram {

	@Override
	protected Object createEntity(final Object nmsWorld, final Location location) {
		return new ArmorStand((ServerLevel) nmsWorld, location.getX(), location.getY(), location.getZ());
	}

	@Override
	protected void sendPackets(final Player player, final Object nmsArmorStand) {
		final ArmorStand nmsStand = (ArmorStand) nmsArmorStand;

		Remain.sendPacket(player, new ClientboundAddEntityPacket(nmsStand, 0, nmsStand.blockPosition()));
		Remain.sendPacket(player, new ClientboundSetEntityDataPacket(nmsStand.getId(), nmsStand.getEntityData().packDirty()));
	}
}
