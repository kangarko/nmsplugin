package org.mineacademy.nms.specific.hologram;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.remain.Remain;

public abstract class NMSHologram {

	public void show(Location location, final Player player, final String... linesOfText) {

		final Object nmsWorld = Remain.getHandleWorld(location.getWorld());

		for (final String line : linesOfText) {
			final Object nmsArmorStand = this.createEntity(nmsWorld, location);
			final ArmorStand armorStand = ReflectionUtil.invoke("getBukkitEntity", nmsArmorStand);

			if (MinecraftVersion.atLeast(MinecraftVersion.V.v1_9))
				armorStand.setInvisible(true);

			Remain.setCustomName(armorStand, line);

			this.sendPackets(player, nmsArmorStand);

			location = location.subtract(0, 0.26, 0);
		}

		// Make a new EntityArmorStand < NOT registered in Bukkit so no Bukkit events

		// send 2 packets > PacketSpawnEntity... + EntityMetadata packet
	}

	protected abstract Object createEntity(Object nmsWorld, Location location);

	protected abstract void sendPackets(Player player, Object nmsArmorStand);

	public static void sendTo(final Location location, final Player player, final String... linesOfText) {
		final NMSHologram hologram;

		if (MinecraftVersion.equals(MinecraftVersion.V.v1_8))
			hologram = new NMSHologram_v1_8();

		else if (MinecraftVersion.equals(MinecraftVersion.V.v1_16))
			hologram = new NMSHologram_v1_16();

		else if ((MinecraftVersion.equals(MinecraftVersion.V.v1_20) && MinecraftVersion.getSubversion() >= 5) || MinecraftVersion.newerThan(MinecraftVersion.V.v1_20))
			hologram = new NMSHologram_Modern();

		else
			throw new FoException("Unsupported Minecraft version " + MinecraftVersion.getFullVersion());

		hologram.show(location, player, linesOfText);
	}
}
