package org.mineacademy.nms.citizens;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.exception.EventHandledException;
import org.mineacademy.fo.model.SimpleTrait;
import org.mineacademy.fo.remain.CompSound;

import net.citizensnpcs.api.npc.NPC;

public class KillBossTrait extends SimpleTrait {

	private Set<UUID> startedQuest = new HashSet<>();
	private Set<UUID> completedQuest = new HashSet<>();

	public KillBossTrait() {
		super("kill-boss");

		// Only call onTick every 10. second
		this.setTickThreshold(20 * 10);
	}

	@Override
	protected void onTick() {
		// called automatically DURING the time this NPC has this trait,
		// it is called as much as you want

		// Send chat messages/bubbles to nearby players depending on their interaction phase

		final NPC npc = this.getNPC();
		final Entity npcEntity = npc.getEntity();
		final int playersNearbyRange = 5;

		if (npcEntity == null)
			return;

		for (final Entity nearby : npcEntity.getWorld().getNearbyEntities(npcEntity.getLocation(), playersNearbyRange, playersNearbyRange, playersNearbyRange))
			if (nearby instanceof Player) {
				final UUID uniqueId = nearby.getUniqueId();
				final Player player = (Player) nearby;

				if (this.completedQuest.contains(uniqueId))
					continue;

				if (this.startedQuest.contains(uniqueId)) {
					Messenger.info(player, RandomUtil.nextItem(
							"Hey {player}, I am waiting for the item!",
							"{player}, go kill the Boss and bring me its item!"));

					continue;
				}

				Messenger.info(player, RandomUtil.nextItem(
						"Hey {player}, I have a mission for you!",
						"{player}, click me to start a mission!"));
			}
	}

	@Override
	public void onClick(final Player player, final ClickType clickType) throws EventHandledException {
		if (clickType != ClickType.RIGHT)
			return;

		final ItemStack handItem = player.getItemInHand();
		final UUID uniqueId = player.getUniqueId();

		if (this.completedQuest.contains(uniqueId))
			this.cancel("You've already completed this quest");

		if (this.startedQuest.contains(uniqueId)) {
			if (ItemUtil.isSimilar(handItem, QuestItems.getKillBossItem())) {

				this.completedQuest.add(uniqueId);
				this.startedQuest.remove(uniqueId);

				PlayerUtil.takeOnePiece(player, handItem);

				Messenger.announce(player, "Congratulations for completing this quest!");
				CompSound.ENTITY_PLAYER_LEVELUP.play(player);
			} else
				cancel("To complete my quest, click me while holding the unlock key from killing a Boss!");

		} else {
			Messenger.success(player, "You've now started a quest! Go kill a Boss and return to me with its unlock key!");

			this.startedQuest.add(uniqueId);
		}
	}

	@Override
	protected void load(final SerializedMap serializedMap) {
		// NPC-specific

		this.startedQuest = serializedMap.getSet("Started_Quest", UUID.class);
		this.completedQuest = serializedMap.getSet("Completed_Quest", UUID.class);
	}

	@Override
	protected void save(final SerializedMap serializedMap) {
		serializedMap.put("Started_Quest", this.startedQuest);
		serializedMap.put("Completed_Quest", this.completedQuest);
	}
}
