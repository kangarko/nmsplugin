package org.mineacademy.nms.citizens;

import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;

public final class QuestItems {

	public static ItemStack getKillBossItem() {
		return ItemCreator.of(CompMaterial.NETHER_STAR,
				"Unlock Key",
				"",
				"Right click holding this key",
				"onto the villager to",
				"complete your quest!")
				.make();
	}
}
