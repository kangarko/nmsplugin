package org.mineacademy.nms.specific.enchant;

import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.enchant.NmsEnchant;
import org.mineacademy.fo.enchant.SimpleEnchantment;

import net.minecraft.server.v1_8_R3.EnchantmentSlotType;
import net.minecraft.server.v1_8_R3.MinecraftKey;

public final class CustomEnchant_v1_8 extends net.minecraft.server.v1_8_R3.Enchantment implements NmsEnchant {

	private static int startId = 63;

	private final SimpleEnchantment simpleEnchantment;

	public CustomEnchant_v1_8(final SimpleEnchantment enchant) {
		super(setupId(), new MinecraftKey(enchant.getNamespacedName()), 1, EnchantmentSlotType.ALL);

		this.simpleEnchantment = enchant;
		this.simpleEnchantment.setLegacyId(this.id);
	}

	/*
	 * A workaround for /reload - Bukkit restarts all classes in our plugin, even static fields, so our enchant
	 * will get re-registered each time on /reload - and we have to manually de-register it from NMS/API
	 */
	private static int setupId() {
		int id = startId++;
		net.minecraft.server.v1_8_R3.Enchantment[] byId = ReflectionUtil.getStaticFieldContent(net.minecraft.server.v1_8_R3.Enchantment.class, "byId");

		if (byId[id] != null)
			byId[id] = null;

		Map<Integer, Enchantment> byIdBukkit = ReflectionUtil.getStaticFieldContent(Enchantment.class, "byId");
		byIdBukkit.remove(id);

		Map<String, Enchantment> byName = ReflectionUtil.getStaticFieldContent(Enchantment.class, "byName");
		byName.remove("UNKNOWN_ENCHANT_" + id);

		return id;
	}

	@Override
	public void register() {
		// Done for you in super()
	}

	@Override
	public Enchantment toBukkit() {
		return Enchantment.getById(this.id);
	}

	@Override
	public int getStartLevel() {
		return this.simpleEnchantment.getStartLevel();
	}

	@Override
	public int getMaxLevel() {
		return this.simpleEnchantment.getMaxLevel();
	}

	@Override
	public int a(final int level) {
		return this.simpleEnchantment.getMinCost(level);
	}

	@Override
	public int b(final int level) {
		return this.simpleEnchantment.getMaxCost(level);
	}

	@Override
	public boolean canEnchant(final net.minecraft.server.v1_8_R3.ItemStack item) {
		return this.simpleEnchantment.canEnchantItem(
				org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack.asBukkitCopy(item)) || super.canEnchant(item);
	}

	@Override
	public boolean a(final net.minecraft.server.v1_8_R3.Enchantment other) {
		final Enchantment bukkitEnch = Enchantment.getById(other.id);

		return bukkitEnch == null || !this.simpleEnchantment.conflictsWith(bukkitEnch);
	}
}