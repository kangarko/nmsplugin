package org.mineacademy.nms.specific.enchant;

import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_19_R3.enchantments.CraftEnchantment;
import org.bukkit.craftbukkit.v1_19_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_19_R3.util.CraftNamespacedKey;
import org.mineacademy.fo.enchant.NmsEnchant;
import org.mineacademy.fo.enchant.SimpleEnchantment;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;

public class CustomEnchant_v1_19 extends Enchantment implements NmsEnchant {

	private final SimpleEnchantment simpleEnchantment;

	protected CustomEnchant_v1_19(final SimpleEnchantment simpleEnchantment) {
		super(Rarity.valueOf(simpleEnchantment.getRarity().name()),
				EnchantmentCategory.valueOf(simpleEnchantment.getTarget().name()),
				EquipmentSlot.values());

		this.simpleEnchantment = simpleEnchantment;
	}

	@Override
	public void register() {
		BuiltInRegistries.ENCHANTMENT.createIntrusiveHolder(this);
		Registry.register(BuiltInRegistries.ENCHANTMENT, simpleEnchantment.getNamespacedName(), this);
		org.bukkit.enchantments.Enchantment.registerEnchantment(new CraftEnchantment(this));
	}

	@Override
	public org.bukkit.enchantments.Enchantment toBukkit() {
		return org.bukkit.enchantments.Enchantment.getByKey(
				new NamespacedKey("minecraft", simpleEnchantment.getNamespacedName())
		);
	}

	@Override
	public int getMinLevel() {
		return this.simpleEnchantment.getStartLevel();
	}

	@Override
	public int getMaxLevel() {
		return this.simpleEnchantment.getMaxLevel();
	}

	@Override
	public int getMinCost(final int level) {
		return this.simpleEnchantment.getMinCost(level);
	}

	@Override
	public int getMaxCost(final int level) {
		return this.simpleEnchantment.getMaxCost(level);
	}

	@Override
	public boolean canEnchant(final ItemStack item) {
		return this.simpleEnchantment.canEnchantItem(CraftItemStack.asBukkitCopy(item)) || super.canEnchant(item);
	}

	@Override
	public boolean isTreasureOnly() {
		return this.simpleEnchantment.isTreasure() || this.isCurse();
	}

	@Override
	public boolean isCurse() {
		return this.simpleEnchantment.isCursed();
	}

	@Override
	public boolean isTradeable() {
		return this.simpleEnchantment.isTradeable();
	}

	@Override
	public boolean isDiscoverable() {
		return this.simpleEnchantment.isDiscoverable();
	}

	@Override
	protected boolean checkCompatibility(final Enchantment other) {
		final ResourceLocation location = BuiltInRegistries.ENCHANTMENT.getKey(other);

		if (location == null)
			return false;

		final NamespacedKey key = CraftNamespacedKey.fromMinecraft(location);
		final org.bukkit.enchantments.Enchantment enchantment = org.bukkit.Registry.ENCHANTMENT.get(key);

		if (enchantment == null)
			return false;

		return !this.simpleEnchantment.conflictsWith(enchantment);
	}
}
