package org.mineacademy.nms.specific.enchant;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.enchant.NmsEnchant;
import org.mineacademy.fo.enchant.SimpleEnchantment;

public final class CustomEnchant_v1_18 extends Enchantment implements NmsEnchant {

	private final SimpleEnchantment enchant;

	public CustomEnchant_v1_18(final SimpleEnchantment enchant) {
		super(new NamespacedKey("minecraft", enchant.getNamespacedName()));

		this.enchant = enchant;
	}

	@Override
	public void register() {
		Enchantment.registerEnchantment(this);
	}

	@Override
	public Enchantment toBukkit() {
		return Enchantment.getByKey(this.getKey());
	}

	@Override
	public String getName() {
		return this.enchant.getName();
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return EnchantmentTarget.valueOf(this.enchant.getTarget().name());
	}

	@Override
	public boolean conflictsWith(final Enchantment other) {
		return this.enchant.conflictsWith(other);
	}

	@Override
	public boolean canEnchantItem(final ItemStack item) {
		return this.enchant.canEnchantItem(item);
	}

	@Override
	public int getStartLevel() {
		return this.enchant.getStartLevel();
	}

	@Override
	public int getMaxLevel() {
		return this.enchant.getMaxLevel();
	}

	@Override
	public boolean isTreasure() {
		return this.enchant.isTreasure();
	}

	@Override
	public boolean isCursed() {
		return this.enchant.isCursed();
	}
}