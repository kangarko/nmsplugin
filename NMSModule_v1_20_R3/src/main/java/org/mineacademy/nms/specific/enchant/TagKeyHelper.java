package org.mineacademy.nms.specific.enchant;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;

public final class TagKeyHelper {

	// Terrible workaround for IDE complaining about Record class being used
	// Record class requires Java 17, but we still want to use Java 8 when coding
	// to make the overall plugin 1.8.8 compatible. We use Java 21 for compiling
	// to ensure 1.20.5+ compatibility and calling this method in a separate
	// library will remove that annoying IDE warning. Weird but works.
	//
	// This should be in theory put in 1.20.5 package but turns out it works
	// fine for 1.20.4 and I dont bother making a new module for 1 method
	public static TagKey<Item> get(String name) {
		return TagKey.create(Registries.ITEM, new ResourceLocation(name));
	}
}
